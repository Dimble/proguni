<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Storico prenotazioni</title>
        <link link rel="stylesheet" type="text/css" href="css/tableStyle.css">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
              crossorigin="anonymous">
    </head>

    <body>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>

        <%
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            if (session.getAttribute("loggedUser") == null) {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/homepage.jsp");
                dispatcher.forward(request, response);
            }
        %>

        <br>
        <h1 align="center"> Storico prenotazioni </h1>

        <div class="container">
            <div class="table-responsive">
                <br>
                <table class="table table-bordered table-striped" id="bookingsHistoryTable">
                    <tr>
                        <th>Studente</th>
                        <th>Corso</th>
                        <th>Giorno</th>
                        <th>Ora</th>
                        <th>Insegnante</th>
                        <th>Stato</th>
                    </tr>
                </table>
            </div>
        </div>

        <!-- table ends-->

        <!-- Javascript and JQuery-->

        <script type="text/javascript">

            //apro il form per la prenotazione 

            $('.li-modal').on('click', function (e) {
                e.preventDefault();
                $('#theModal').modal('show').find('.modal-content').load($(this).attr('href'));
            });


            $(document).ready(function () {
                $.getJSON("BookingController?action=booking_history", function (data) {
                    var bookingsHistory_data = '';
                    $.each(data, function (key, value) {
                        bookingsHistory_data += '<tr>';
                        bookingsHistory_data += '<td>' + value.studentDatas + '</td>';
                        bookingsHistory_data += '<td>' + value.courseName + '</td>';
                        bookingsHistory_data += '<td>' + value.day + '</td>';
                        bookingsHistory_data += '<td>' + value.hour + '</td>';
                        bookingsHistory_data += '<td>' + value.teacherDatas + '</td>';
                        bookingsHistory_data += '<td>' + value.status + '</td>';
                        bookingsHistory_data += '</tr>';
                    });
                    $('#bookingsHistoryTable').append(bookingsHistory_data);
                });
            });
            
            document.getElementById('bookingBtn').style.visibility='hidden';

        </script>
    </body>
</html>
