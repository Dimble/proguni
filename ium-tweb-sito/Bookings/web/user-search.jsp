<%@include file="header.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Catalog test</title>

        <!--Table Style-->
        <link link rel="stylesheet" type="text/css" href="css/tableStyle.css">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS  forse da aggiornare a nuova versione-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    </head>

    <body>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>

        <br>
        <h1 align="center"> Catalogo dei corsi disponibili </h1>

        <div class="container">
            <div class="table-responsive">
                <br>
                <table class="table table-bordered table-striped" id="catalogTable">
                    <tr>
                        <th>Nome del corso</th>
                        <th>Insegnante</th>
                    </tr>
                </table>
            </div>
        </div>

        <!-- Javascript and JQuery-->

        <script type="text/javascript">
            $(document).ready(function () {
                $.getJSON("BookingController?action=search_course", function (data) {
                    var catalog_data = '';
                    $.each(data, function (key, value) {
                        catalog_data += '<tr>';
                        catalog_data += '<td>' + value.courseName + '</td>';
                        catalog_data += '<td>' + value.teacherDatas + '</td>';
                        catalog_data += '</tr>';
                    });
                    $('#catalogTable').append(catalog_data);
                });
            });
        </script>
    </body>
</html>
