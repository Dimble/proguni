<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/bookingForm.css">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
              crossorigin="anonymous">

        <title>JSP Page</title>
    </head>
    <body>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>

        <form action="BookingController" method="GET">
            <div class="modal-header">
                <h5 class="modal-title">Prenota subito la tua lezione!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <select id="course">
                    <option value="" disabled selected>Seleziona il corso</option>

                </select>
                <br>
                <br>
                <select id="teacher">
                    <option value="" disabled selected>Seleziona l'insegnante</option>
                </select>
                <br>
                <br>
                <select id="days" disabled>
                    <option value="" disabled selected>Seleziona il giorno</option>
                    <option >Lunedi</option>
                    <option >Martedi</option>
                    <option >Mercoledi</option>
                    <option >Giovedi</option>
                    <option >Venerdi</option>
                </select>
                <br>
                <br>
                <select id='hour'>
                    <option value="" disabled selected>Seleziona l'ora</option>
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" id="book" class="btn btn-primary">Prenota</button>
            </div>
        </form>

        <script type="text/javascript">

            $(document).ready(function () {
                $.getJSON("BookingController?action=show_courses&option=associatio", function (data) {
                    var courses_data = '';
                    $.each(data, function (key, value) {
                        courses_data += '<option>' + value.courseName + '</option>';
                    });
                    $("#course").append(courses_data);
                });
            });

            $("#course").change(function () {
                $.ajax({
                    type: 'GET',
                    cache: false,
                    url: 'BookingController',
                    data: {
                        action: "show_teachers",
                        option: "form",
                        course: $("#course").val()
                    },
                    success: function (data) {
                        teacher = JSON.parse(data);
                        var option;
                        $("#teacher").empty();
                        for (var i in teacher) {
                            option = '<option>' + teacher[i].firstNameLastName + '</option>';
                            $("#teacher").append(option);
                        }
                    },
                    error: function (xhr) {
                        alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                    }
                });
            });

            $("#teacher").focus(function () {
                document.getElementById("days").disabled = (this.value === '0');
            });

            $("#teacher").change(function () {
                $('#days')[0].selectedIndex = 0;
            });

            $("#days").change(function () {
                $.ajax({
                    type: 'GET',
                    cache: false,
                    url: 'BookingController',
                    data: {
                        action: "show_available_hours",
                        teacherName: $("#teacher>option:selected").val(),
                        day: $("#days>option:selected").val()
                    },
                    success: function (data) {
                        hours = JSON.parse(data);
                        var option;
                        $("#hour").empty();
                        for (var i in hours) {
                            option = '<option>' + hours[i] + '</option>';
                            $("#hour").append(option);
                        }
                    }
                });
            });

            $("#book").click(function () {
                if ($("#course").val() === null || $("#teacher").val() === null || $("#days").val() === null || $("#hour").val() === null) {
                    $.notify("Attenzione, è obbligatorio compilare tutti i campi.", "warn");
                } else {
                    $.ajax({
                        type: 'POST',
                        url: 'BookingController',
                        data: {
                            action: "book_lesson",
                            username: "${loggedUser}",
                            day: $("#days").val(),
                            hour: $("#hour").val(),
                            course: $("#course").val(),
                            teacher: $("#teacher").val(),
                            status: "Attiva"
                        },
                        success: function (res) {
                            ris = JSON.parse(res);
                            if (ris !== 0) {
                                $.notify("Prenotazione effettutata con successo.", "success");
                            } else {
                                $.notify("Attenzione! Prenotazione fallita, hai già una prenotazione attiva nello slot selezionato.", "error");
                            }
                        }
                    });
                }
            });

        </script>
    </body>
</html>
