<%@page import="java.util.List"%>
<%@include file="header.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reservations</title>
        <link link rel="stylesheet" type="text/css" href="css/tableStyle.css">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
              crossorigin="anonymous">
    </head>

    <body>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>

        <%
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            if (session.getAttribute("loggedUser") == null) {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/homepage.jsp");
                dispatcher.forward(request, response);
            }
        %>

        <br>
        <h1 align="center"> Le mie prenotazioni </h1>

        <div class="container">
            <div class="table-responsive">
                <br>
                <table class="table table-bordered table-striped" id="userReservationsTable">
                    <tr>
                        <th>Nome del corso</th>
                        <th>Giorno</th>
                        <th>Ora</th>
                        <th>Insegnante</th>
                        <th>Stato</th>
                        <th>Azione</th>
                    </tr>
                </table>
            </div>
        </div>


        <!-- Javascript and JQuery-->
        <script type="text/javascript">

            $(document).ready(function () {
                $.getJSON("BookingController?action=user_reservations", function (data) {
                    var user_reservations = '';
                    $.each(data, function (key, value) {
                        user_reservations += '<tr>';
                        user_reservations += '<td>' + value.courseName + '</td>';
                        user_reservations += '<td>' + value.day + '</td>';
                        user_reservations += '<td>' + value.hour + '</td>';
                        user_reservations += '<td>' + value.teacherDatas + '</td>';
                        user_reservations += '<td>' + value.status + '</td>';
                        user_reservations += '<td>' + '<a id="deleteRequestBtn"' + 'href="BookingController?action=delete_reservation&reservationId=' + value.bookingId + '">' + "Annulla prenotazione" + '</a>' + '</td>';
                        user_reservations += '</tr>';
                    });
                    $('#userReservationsTable').append(user_reservations);
                });
            });
            
            document.getElementById('bookingBtn').style.visibility='hidden';

        </script>
    </body>
</html>
