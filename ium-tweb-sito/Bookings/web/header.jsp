<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" type="text/css" href="css/homepageStyle.css">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS  forse da aggiornare a nuova versione-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

    </head>

    <body>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">MyHomepage</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <c:choose>
                    <c:when test="${empty sessionScope.loggedUser}">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="BookingController?action=homepage">Home <span
                                        class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="BookingController?action=catalog_page">I nostri corsi <span
                                        class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">Download the App!</a>
                            </li>
                        </ul>
                    </c:when>
                    <c:when test="${!empty sessionScope.loggedUser}">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="BookingController?action=homepage">Home <span
                                        class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="BookingController?action=catalog_page">I nostri corsi <span
                                        class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="BookingController?action=user_reservations_page">Le mie
                                    prenotazioni <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">Download the App!</a>
                            </li>
                            <c:choose>
                                <c:when test="${sessionScope.role == 'admin'}">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="BookingController?action=admin_operations" style="color:#e65100; ">Pannello
                                            Amministrazione <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item active">
                                        <a class="nav-link" href="BookingController?action=history_page" style="color:#e65100;">Storico
                                            prenotazioni <span class="sr-only">(current)</span></a>
                                    </li>
                                </c:when>
                            </c:choose>
                        </c:when>
                    </c:choose>
                </ul>
                <div class="form-inline my-2 my-lg-0">
                    <div id="loginModal" class="modal fade text-center">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            </div>
                        </div>
                    </div>
                    <c:choose>
                        <c:when test="${empty sessionScope.loggedUser}">
                            <input id="loginBtn" type="button" value="Log In" href="login.jsp" class='li-modal'>
                        </c:when>
                        <c:when test="${!empty sessionScope.loggedUser}">
                            <div id="outer">
                                <div class="inner"><input id="bookingBtn" type ="button" value="Prenota una lezione" href="BookingController?action=book" class='li-modal'></div>
                                <div class="inner"><input id="logoutBtn" type="button"  value="Log Out"></div>
                            </div>
                        </c:when>   
                    </c:choose>
                </div>
            </div>
        </nav>


        <!--nav bar ends-->



        <script type="text/javascript">

            //apro il form per il login 
            $('.li-modal').on('click', function (e) {
                e.preventDefault();
                $('#loginModal').modal('show').find('.modal-content').load($(this).attr('href'));
            });

            //Apro il form per la prenotazione 
            $('.li-modal').on('click', function (e) {
                e.preventDefault();
                $('#theModal').modal('show').find('.modal-content').load($(this).attr('href'));
            });

            $(document).ready(function () { //waiting for the document to be loaded and than execute the ajax code
                $("#logoutBtn").on('click', function () {
                    $.ajax({
                        type: 'GET',
                        cache: false,
                        url: 'BookingController',
                        data: {
                            action: "logout"
                        },
                        success: function (response) {
                            res = JSON.parse(response);
                            if (res === "logout") {
                                window.location.href = "homepage.jsp";
                            }
                        }
                    });
                });
            });

        </script>
    </body>
</html>
