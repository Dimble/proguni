<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/modalLogin.css">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS  forse da aggiornare a nuova versione-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    </head>

    <body>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>

        <div class="modal-header">
            <h5 class="modal-title">Login</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form>
                <div>
                    <input type="text" name="Username" required="" id='username'>
                    <label>Username</label>
                </div>
                <div>
                    <input type="password" name="Password" required="" id='password'>
                    <label>Password</label>
                </div>
                <input type="button" value="Log In" id="validate">
                <p id="forgotPswTextFooter"> <a href="#" style="color:#ff7043">Password dimenticata?</a> </p>
                <br>
            </form>
        </div>
        <div class="modal-footer">
            <p id="loginBtnTextFooter"> Non hai un account? <a href="#" style="color:#ff7043"> Registrati subito! </a></p>
        </div>


        <script type="text/javascript">

            $(document).ready(function () { //waiting for the document to be loaded and than execute the ajax code
                $("#validate").on('click', function () {
                    $.ajax({
                        type: 'POST',
                        cache: false,
                        url: 'BookingController',
                        data: {
                            action: "login_request",
                            username: $("#username").val(),
                            password: $("#password").val()
                        },
                        success: function (response) {
                            res = JSON.parse(response);
                            if (res === 1) {
                                window.location.href = "homepage.jsp";
                            } else {
                                $.notify("Errore di login: ricontrolla i dati inseriti.", "error");
                            }
                        }
                    });
                });
            });


        </script>
    </body>
</html>
