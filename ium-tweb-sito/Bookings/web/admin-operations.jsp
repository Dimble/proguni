<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/adminStyle.css">
        <title>Admin operations</title

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
              crossorigin="anonymous">
    </head>
    <body>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>

        <%
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            if (session.getAttribute("loggedUser") == null) {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/homepage.jsp");
                dispatcher.forward(request, response);
            }
        %>

        <header>
            <h1 id="h1" align="center">Pannello di amministrazione</h1>
        </header>

        <section>
            <div id="body-wrapper">
                <div class="borderedCourseOperations">
                    <!-- INSERT COURSE -->
                    <h2 id="insertCourseH2">Inserisci un corso</h2>
                    <div class="form-inline">
                        <div class="form-group mb-2">
                            <input type="text" class="form-control mb-2" id="courseId" placeholder="Id del corso">
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                            <input type="text" class="form-control mb-2" id="courseName" placeholder="Nome del corso">
                        </div>
                        <button id="insertCourseBtn" type="submit" class="search-btn">Inserisci</button>
                    </div>

                    <!-- DELETE COURSE -->
                    <h2 id="deleteCourseH2">Cancella/Attiva un corso</h2>
                    <div>
                        <select id="coursesOptions" class="custom-select my-1 mr-sm-2">
                            <option value="" disabled selected>Seleziona il corso</option>

                        </select>
                        <button id="deleteCourseBtn" type="submit" class="search-btn">Elimina</button>
                        <button id="reactivateCourseBtn" type="submit" class="search-btn">Attiva</button>
                    </div>
                </div>
                <!-- END OF COURSES PANEL -->

                <div class="borderedTeacherOperations">
                    <!-- INSERT TEACHER -->
                    <h2 id="insertTeacherH2">Inserisci insegnante</h2>
                    <div class="form-inline">
                        <div class="form-group mb-2">
                            <input type="text" class="form-control mb-2" id="teacherId" placeholder="Id insegnante">
                        </div>
                        <div class="form-group mx-sm-3 mb-2">
                            <input type="text" class="form-control mb-2" id="teacherName" placeholder="Nome insegnante">
                        </div>
                        <button id="insertTeacherBtn" type="submit" class="search-btn">Inserisci</button>
                    </div>

                    <!-- DELETE TEACHER -->
                    <h2 id="deleteTeacherH2">Cancella/Attiva insegnante</h2>
                    <div>
                        <select id="deleteShowTeachers" class="custom-select my-1 mr-sm-2">
                            <option value="" disabled selected>Seleziona un docente</option>

                        </select>
                        <button id="deleteTeacherBtn" type="submit" class="search-btn">Elimina</button>
                        <button id="reactivateTeacherBtn" type="submit" class="search-btn">Attiva</button>
                    </div>
                </div>

                <div class="borderedAssociationOperations">
                    <!-- INSERT Association -->
                    <h2 id="insertAssociationH2">Inserisci associazione corso-docente</h2>
                    <div class="form-inline">
                        <select id="showCoursesIns" class="custom-select my-1 mr-sm-2">
                            <option value="" disabled selected>Seleziona un corso</option>

                        </select>
                        <select id="showTeachers" class="custom-select my-1 mr-sm-2">
                            <option value="" disabled selected>Seleziona un docente</option>

                        </select>
                        <button id="addAssociationBtn" type="submit" class="search-btn">Inserisci</button>
                    </div>

                    <!-- DELETE Association -->
                    <h2 id="deleteAssociationH2">Cancella associazione corso-docente</h2>
                    <div>
                        <select id="showCoursesDel" class="custom-select my-1 mr-sm-2">
                            <option value="" disabled selected>Seleziona un corso</option>

                        </select>
                        <select id="showTeachersDel" class="custom-select my-1 mr-sm-2">
                            <option value="" disabled selected>Seleziona un docente</option>

                        </select>
                        <button id="deleteAssociationBtn" type="submit" class="search-btn">Elimina</button>
                    </div>
                </div>
            </div>
        </section>
        <footer>

        </footer>

        <!-- Javascript and JQuery-->

        <script type="text/javascript">

            // courses operations

            $(document).ready(function () {
                $.getJSON("BookingController?action=show_courses&option=association", function (data) {
                    var courses_data = '';
                    $.each(data, function (key, value) {
                        courses_data += '<option value = "' + value.id + '">' + value.courseName + '</option>';
                    });
                    $("#coursesOptions").append(courses_data);
                    $("#showCoursesIns").append(courses_data);
                    $("#showCoursesDel").append(courses_data);
                });
            });

            // field per cancellazione o riattivazione 

            $("#insertCourseBtn").click(function () {
                var field = document.getElementById("courseName").value;
                if ($("#courseId").val() === "" || $("#courseName").val() === "") {
                    $.notify("Attenzione, è obbligatorio compilare entrambi i campi.", "warn");
                } else if (!checkPattern(field)) {
                    $.notify("Errore - il nome del corso non può contenere caratteri speciali. \nEsempio: Sistemi Intelligenti", "error");
                } else {
                    $.ajax({
                        type: 'POST',
                        url: 'BookingController',
                        data: {
                            action: "insert_course",
                            courseId: $("#courseId").val(),
                            courseName: $("#courseName").val()
                        },
                        success: function (res) {
                            ris = JSON.parse(res);
                            if (ris !== 0) {
                                $.notify("Inserimento effettutato con successo.", "success");
                            } else {
                                $.notify("Inserimento del corso non riuscito.", "error");
                            }
                        }
                    });
                }
            });

            $("#deleteCourseBtn").click(function () {
                $.ajax({
                    type: 'POST',
                    url: 'BookingController',
                    data: {
                        action: "delete_course",
                        courseId: $("#coursesOptions>option:selected").attr("value")
                    },
                    success: function (res) {
                        ris = JSON.parse(res);
                        if (ris !== 0) {
                            $.notify("Cancellazione effettutata con successo.", "success");
                        } else {
                            $.notify("Cancellazione del corso non riuscita.", "error");
                        }
                    }
                });
            });

            $("#reactivateCourseBtn").click(function () {
                $.ajax({
                    type: 'POST',
                    url: 'BookingController',
                    data: {
                        action: "reactivate_data",
                        id: $("#coursesOptions>option:selected").attr("value"),
                        option: "course"
                    },
                    success: function (res) {
                        ris = JSON.parse(res);
                        if (ris !== 0) {
                            $.notify("Riattivazione del corso effettutata con successo.", "success");
                        } else {
                            $.notify("Riattivazione del corso non riuscita.", "error");
                        }
                    }
                });
            });

            $("#reactivateTeacherBtn").click(function () {
                $.ajax({
                    type: 'POST',
                    url: 'BookingController',
                    data: {
                        action: "reactivate_data",
                        id: $("#deleteShowTeachers>option:selected").attr("value"),
                        option: "teacher"
                    },
                    success: function (res) {
                        ris = JSON.parse(res);
                        if (ris !== 0) {
                            $.notify("Riattivazione dell'insegnante effettutata con successo.", "success");
                        } else {
                            $.notify("Riattivazione dell'insegnante non riuscita.", "error");
                        }
                    }
                });
            });

            // TEACHERS OPERATIONS
            $(document).ready(function () {
                $.getJSON("BookingController?action=show_teachers&option=allTeachers", function (data) {
                    var teachers_data = '';
                    $.each(data, function (key, value) {
                        teachers_data += '<option value = "' + value.teacherId + '">' + value.firstNameLastName + '</option>';
                    });
                    $("#deleteShowTeachers").append(teachers_data);
                });
            });

            $("#insertTeacherBtn").click(function () {
                var field = document.getElementById("teacherName").value;
                if ($("#teacherId").val() === "" || $("#teacherName").val() === "") {
                    $.notify("Attenzione, è obbligatorio compilare entrambi i campi.", "warn");
                } else if (!checkPattern(field)) {
                    $.notify("Errore - il nome dell'insegnante non può contenere caratteri speciali.", "error");
                } else {
                    $.ajax({
                        type: 'POST',
                        url: 'BookingController',
                        data: {
                            action: "insert_teacher",
                            teacherId: $("#teacherId").val(),
                            teacherName: $("#teacherName").val()
                        },
                        success: function (res) {
                            ris = JSON.parse(res);
                            if (ris !== 0) {
                                $.notify("Inserimento effettutato con successo.", "success");
                            } else {
                                $.notify("Inserimento dell'insegnante non riuscito.", "error");
                            }
                        }
                    });
                }
            });

            // insegnanti dell'associazione 
            $("#showCoursesIns").change(function () {
                $.ajax({
                    type: 'GET',
                    cache: false,
                    url: 'BookingController',
                    data: {
                        action: "show_teachers",
                        courseId: $("#showCoursesIns>option:selected").attr("value"),
                        option: "association"
                    },
                    success: function (data) {
                        teachers = JSON.parse(data);
                        if (teachers !== null) {
                            var option;
                            $("#showTeachers").empty();
                            for (var i in teachers) {
                                option = '<option value = "' + teachers[i].teacherId + '">' + teachers[i].firstNameLastName + '</option>';
                                $("#showTeachers").append(option);
                            }
                        } else {
                            $.notify("Impossibile trovare la lista dei docenti.", "error");
                        }
                    }
                });
            });

            $("#deleteTeacherBtn").click(function () {
                $.ajax({
                    type: 'POST',
                    url: 'BookingController',
                    data: {
                        action: "delete_teacher",
                        teacherId: $("#deleteShowTeachers>option:selected").attr("value")
                    },
                    success: function (res) {
                        ris = JSON.parse(res);
                        if (ris !== 0) {
                            $.notify("Cancellazione effettutata con successo.", "success");
                        } else {
                            $.notify("Cancellazione dell'insegnante non riuscita.", "error");
                        }
                    }
                });
            });

            $("#addAssociationBtn").click(function () {
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: 'BookingController',
                    data: {
                        action: "insert_association",
                        courseId: $("#showCoursesIns>option:selected").attr("value"),
                        teacherId: $("#showTeachers>option:selected").attr("value")
                    },
                    success: function (data) {
                        ris = JSON.parse(data);
                        if (ris !== 0) {
                            $.notify("Inserimento dell'associazione effettutata con successo.", "success");
                        } else {
                            $.notify("Inserimento dell'associazione non riuscito.", "error");
                        }
                    }
                });
            });

            $("#showCoursesDel").change(function () {
                $.ajax({
                    type: 'GET',
                    cache: false,
                    url: 'BookingController',
                    data: {
                        action: "show_teachers",
                        courseId: $("#showCoursesDel>option:selected").attr("value"),
                        option: "association-del"
                    },
                    success: function (data) {
                        teachers = JSON.parse(data);
                        if (teachers !== null) {
                            var option;
                            $("#showTeachersDel").empty();
                            for (var i in teachers) {
                                option = '<option value = "' + teachers[i].teacherId + '">' + teachers[i].firstNameLastName + '</option>';
                                $("#showTeachersDel").append(option);
                            }
                        } else {
                            $.notify("Impossibile trovare la lista dei docenti.", "error");
                        }
                    }
                });
            });

            $("#deleteAssociationBtn").click(function () {
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: 'BookingController',
                    data: {
                        action: "delete_association",
                        courseId: $("#showCoursesDel>option:selected").attr("value"),
                        teacherId: $("#showTeachersDel>option:selected").attr("value")
                    },
                    success: function (data) {
                        ris = JSON.parse(data);
                        if (ris !== 0) {
                            $.notify("Cancellazione dell'associazione effettutata con successo.", "success");
                        } else {
                            $.notify("Cancellazione dell'associazione non riuscita.", "error");
                        }
                    }
                });
            });


            // checks for valid imputs
            function checkPattern(data) {
                var re = /^(?=.{1,40}$)[a-zA-Z]+(?:[-\s][a-zA-Z]+)*$/;
                return  data.match(re);
            }
        </script>
    </body>

</html>
