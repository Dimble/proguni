<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<!DOCTYPE html>
<html>

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>MyPage</title>
        <link rel="stylesheet" type="text/css" href="css/homepageStyle.css">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS  forse da aggiornare a nuova versione-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

    </head>

    <body>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>

        <div class="jumbotron" style="background-image: linear-gradient(rgba(0,0,0,0.3),rgba(0,0,0,0.3)),url('img/jumba.png');">
            <form>
                <h1 id=h1ID>Prenota subito la tua lezione!</h1>
                <div class="form-box">
                    <input id="searchBar" class="search-field skills" name="search" type="text" placeholder="Materia (Es. Fisica)" class="form-control" />
                    <button id="searchBtn" class="search-btn" type="button">Cerca</button>
                </div>
            </form>
        </div>

        <div class="content">
            <h2 id="h2ID">Scegli tra i nostri insegnati di scuole e università!</h2>
            <br>
        </div>

        <div class="container">
            <div class="card-deck">
                <div class="card text-center" >
                    <img IMG class="searchLent" src="img/search.png" alt="Avatar" style="width:20%">
                    <div class="container" >
                        <h4><b>Trova l'insegnante perfetto</b></h4> 
                        <p>Confronta centinaia di insegnanti in base ai criteri che ti interessano.</p> 
                    </div>
                </div>
                <div class="card text-center">
                    <img IMG class="calendar" src="img/calendar.png" alt="Avatar" style="width:20%">
                    <div class="container">
                        <h4><b>Prenota la tua lezione</b></h4> 
                        <p>Scegli giorno ed ora della lezione e prenota subito online. </p> 
                    </div>
                </div>
                <div class="card text-center">
                    <img IMG class="prize" src="img/icons8-gold-medal-48.png" alt="Avatar" style="width:20%">
                    <div class="container">
                        <h4><b>Migliora i tuoi voti</b></h4> 
                        <p>Bastano poche ore di lezione per migliorare i tuoi voti e raggiungere i tuoi obbiettivi.</p> 
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div id="footer">
                <h2 id="h3ID">Scarica l'App!</h2>
                <p><a href="#"><img id="appIcon" src="img/appstore.png" width="40" height="60"></a></p>
            </div>
        </footer>

    </body>

</html>
