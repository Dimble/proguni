package com;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fortunato Cogliandro
 */
public class OperationsDbUtil {

    //private DataSource dataSource;
    private final String url;
    private final String user;
    private final String psw;

    public OperationsDbUtil(String url, String user) {
        this.url = url;
        this.user = user;
        this.psw = "";
        registerDriver();
    }

    // registering JDBC Drivers
    public static void registerDriver() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Get Available private lessons.
     *
     * @return a list containing available private lessons.
     * @throws SQLException
     */
    public List<Operation> getAvailablePrivateLessons() throws SQLException {
        List<Operation> availablePrivateLessons = new ArrayList<>();

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;

        try {
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);
            //sql statement
            String catalog = "SELECT courses.CourseName, teachers.FirstNameLastName FROM courses join coursesteaching"
                    + " on courses.id = CourseId join teachers on coursesteaching.TeacherId = teachers.TeacherId WHERE courses.Available='Yes' AND teachers.Available='Yes' ORDER BY CourseName ASC";
            myStatement = myConnection.createStatement();
            //execute query
            myResultSet = myStatement.executeQuery(catalog);
            //process result set
            while (myResultSet.next()) {
                //retrive data from result set row
                String courseName = myResultSet.getString("CourseName");
                String teacherDatas = myResultSet.getString("FirstNameLastName");

                //create new operation obj 
                Operation tmpData = new Operation(courseName, teacherDatas);
                //add it into list of student
                availablePrivateLessons.add(tmpData);
            }
            return availablePrivateLessons;
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }

    }

    /**
     * Get courses
     *
     * @param option - this param is used to check if the user is requesting the
     * full list of courses or only the available ones.
     * @return the requested list of courses.
     * @throws SQLException
     */
    public List<Course> getCourses(String option) throws SQLException {
        List<Course> coursesList = new ArrayList<>();
        String courses = null;

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;
        try {
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);

            //sql statement
            if (!option.equals("association")) {
                courses = "SELECT * from courses WHERE Available='Yes'";
            } else {
                courses = "SELECT * from courses";
            }

            myStatement = myConnection.createStatement();
            //execute query
            myResultSet = myStatement.executeQuery(courses);
            //process result set
            while (myResultSet.next()) {
                //retrive data from result set row
                Course courseDatas = new Course(
                        myResultSet.getString("Id"),
                        myResultSet.getString("CourseName"),
                        myResultSet.getString("Available")
                );
                //add it into list of courses
                coursesList.add(courseDatas);
            }
            return coursesList;
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }
    }

    /**
     * Get the list of teachers
     *
     * @param course - needed to filter the teachers who teach in the specif
     * course
     * @param courseId - needed to filter the teachers who teach in the specif
     * course
     * @param option - this param is used to check if the user is requesting the
     * full list of teachers or any filter is required (ex. delete an
     * association)
     * @return the requested list of teachers
     * @throws SQLException
     */
    public List<Teacher> getTeachers(String course, String courseId, String option) throws SQLException {
        List<Teacher> teachersList = new ArrayList<>();
        String teachers = null;

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;
        try {
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);
            //sql statement

            switch (option) {
                case "association":
                    teachers = "SELECT * FROM teachers WHERE TeacherId NOT IN " + "(SELECT coursesteaching.TeacherId FROM coursesteaching join teachers "
                            + "ON coursesteaching.TeacherId = teachers.TeacherId WHERE coursesteaching.CourseId ='" + courseId + "')";
                    break;
                case "association-del":
                    teachers = "SELECT teachers.TeacherId, FirstNameLastName, Available FROM coursesteaching join "
                            + " teachers ON coursesteaching.TeacherId = teachers.TeacherId WHERE coursesteaching.CourseId = '" + courseId + "'";
                    break;
                case "allTeachers":
                    teachers = "SELECT * from teachers";
                    break;
                default:
                    teachers = "SELECT teachers.TeacherId, teachers.FirstNameLastName, teachers.Available FROM courses join coursesteaching ON courses.Id = coursesteaching.CourseId "
                            + " join teachers ON teachers.TeacherId = coursesteaching.TeacherId WHERE CourseName = '" + course + "'" + " AND teachers.Available='Yes'";
                    break;
            }

            myStatement = myConnection.createStatement();
            //execute query
            myResultSet = myStatement.executeQuery(teachers);
            //process result set
            while (myResultSet.next()) {
                //retrive data from result set row
                Teacher teacherDatas = new Teacher(
                        myResultSet.getString("TeacherId"),
                        myResultSet.getString("FirstNameLastName"),
                        myResultSet.getString("Available")
                );
                //add it into list of courses
                teachersList.add(teacherDatas);
            }
            return teachersList;
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }
    }

    /**
     * Get the avaiable hours for booking.
     *
     * @param teacherName - the name of the teacher whom we are trying to book
     * with.
     * @param day - the day we want to book out lesson.
     * @return - the available hours.
     * @throws SQLException
     */
    List<String> getAvailableHours(String teacherName, String day) throws SQLException {
        List<String> hoursList = new ArrayList<>();

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;
        try {
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);
            //sql statement
            String getHours = "SELECT Hour from bookings WHERE Teacher='" + teacherName + "'" + "AND Day ='" + day + "'"
                    + " AND Status='Attiva'";
            myStatement = myConnection.createStatement();
            //execute query
            myResultSet = myStatement.executeQuery(getHours);
            //process result set
            while (myResultSet.next()) {
                //retrive data from result set row
                String hour = myResultSet.getString("Hour");
                //add it into list of student
                hoursList.add(hour);
            }
            return hoursList;
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }
    }

    /**
     * Get the specified user reservations.
     *
     * @param username - the user who's requesting the reservations.
     * @return - the user reservations.
     * @throws SQLException
     */
    List<Operation> getUserReservations(String username) throws SQLException {
        List<Operation> userReservationsList = new ArrayList<>();

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;
        try {
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);
            //sql statement
            String courses = "SELECT BookingId,CourseName,Day,Hour,Teacher,Status FROM bookings WHERE bookings.Username='" + username + "'";
            myStatement = myConnection.createStatement();
            //execute query
            myResultSet = myStatement.executeQuery(courses);
            //process result set
            while (myResultSet.next()) {
                //retrive data from result set row
                int bookingId = myResultSet.getInt("BookingId");
                String courseName = myResultSet.getString("CourseName");
                String day = myResultSet.getString("Day");
                String hour = myResultSet.getString("Hour");
                String teacherDatas = myResultSet.getString("Teacher");
                String status = myResultSet.getString("Status");

                //create new operation obj 
                Operation tmpData = new Operation(bookingId, courseName, day, hour, teacherDatas, status);
                //add it into list of student
                userReservationsList.add(tmpData);
            }
            return userReservationsList;
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }
    }

    /**
     * Get all users reservations.
     *
     * @return - all users reservations.
     * @throws SQLException
     */
    List<Operation> getbookingHistory() throws SQLException {
        List<Operation> bookingHistory = new ArrayList<>();

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;
        try {
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);
            //sql statement
            String getBookings = "SELECT BookingId, bookings.StudentData, CourseName, Day, Hour, Teacher, Status"
                    + " FROM bookings join account ON bookings.Username = account.username ORDER BY StudentData ASC";
            myStatement = myConnection.createStatement();
            //execute query
            myResultSet = myStatement.executeQuery(getBookings);
            //process result set
            while (myResultSet.next()) {
                //retrive data from result set row
                int bookingId = myResultSet.getInt("BookingId");
                String studentDatas = myResultSet.getString("StudentData");
                String courseName = myResultSet.getString("CourseName");
                String day = myResultSet.getString("Day");
                String hour = myResultSet.getString("Hour");
                String teacherDatas = myResultSet.getString("Teacher");
                String status = myResultSet.getString("Status");

                //create new operation obj 
                Operation tmpData = new Operation(bookingId, studentDatas, courseName, day, hour, teacherDatas, status);
                //add it into list of student
                bookingHistory.add(tmpData);
            }
            return bookingHistory;
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }
    }

    /**
     * Book a lesson.
     *
     * @param username - who's booking.
     * @param day - specified day.
     * @param hour specified hour.
     * @param course - specified course.
     * @param teacher - specified teacher.
     * @param status - the status of the reservation.
     *
     * @return 1 on success else 0.
     */
    int bookLesson(String username, String day, String hour, String course, String teacher, String status) {
        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;

        int success = 0;

        try {
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);

            //sql statement
            String bookLesson = "INSERT INTO bookings (Username, Day, Hour, CourseName, Teacher, Status) VALUES ('" + username
                    + "'," + "'" + day + "'," + "'" + hour + "'," + "'" + course + "'," + "'" + teacher + "'," + "'" + status + "'" + ")";

            myStatement = myConnection.createStatement();
            success = myStatement.executeUpdate(bookLesson);

        } catch (SQLException ex) {
            Logger.getLogger(OperationsDbUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }
        return success;
    }

    /**
     * Delete a reservation.
     *
     * @param bookingId - the id of the reservation we want to delete.
     */
    void deleteReservation(int bookingId) {

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;

        try {
            int reservationId = bookingId;
            String deletedStatus = "Cancellata";
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);

            //sql statement
            String deleteReservation = "UPDATE IGNORE bookings SET Status =" + "'" + deletedStatus + "'" + "WHERE BookingId=" + reservationId;
            myStatement = myConnection.createStatement();
            myStatement.executeUpdate(deleteReservation);

        } catch (SQLException ex) {
            Logger.getLogger(OperationsDbUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }
    }
    /**
     * Login a user.
     * 
     * @param username - the inserted username.
     * @param password the inserted password.
     * 
     * @return - the role (student or admin) of the user who's logging in, null if the user does not exist.
     * @throws SQLException 
     */
    String validateUser(String username, String password) throws SQLException {
        String result = null;
        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;
        try {
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);
            //sql statement
            String login = "SELECT role FROM account WHERE username='" + username + "' AND password='" + password + "'";
            myStatement = myConnection.createStatement();
            //execute query
            myResultSet = myStatement.executeQuery(login);
            //process result set
            while (myResultSet.next()) {
                //retrive data from result set row
                result = myResultSet.getString("role");
            }
            if (result != null) {
                return result;
            }
            return null;
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }
    }

    // Admin operations

    
    /**
     * Insert a course.
     * 
     * @param courseId - the course id.
     * @param courseName - the course name.
     * @param available - the status of the course.
     * @return 1 if insert successful else 0.
     */
    int insertCourse(String courseId, String courseName, String available) {
        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;

        int success = 0;

        try {
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);

            //sql statement
            String insertCourse = "INSERT INTO courses (Id, CourseName, Available) VALUES ('" + courseId + "'," + "'" + courseName + "'," + "'" + available + "'" + ")";

            myStatement = myConnection.createStatement();
            success = myStatement.executeUpdate(insertCourse);

        } catch (SQLException ex) {
            Logger.getLogger(OperationsDbUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }
        return success;
    }
    
    /**
     * Insert a new teacher.
     * 
     * @param teacherId - the teacher id.
     * @param teacherName - the teacher name.
     * @param available - the status of the teacher.
     * @return 1 if insert successful else 0.
     */

    int insertTeacher(String teacherId, String teacherName, String available) {
        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;

        int success = 0;

        try {
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);

            //sql statement
            String insertTeacher = "INSERT INTO teachers (TeacherId, FirstNameLastName, Available) VALUES ('" + teacherId + "'," + "'" + teacherName + "'," + "'" + available + "'" + ")";

            myStatement = myConnection.createStatement();
            success = myStatement.executeUpdate(insertTeacher);

        } catch (SQLException ex) {
            Logger.getLogger(OperationsDbUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }
        return success;
    }
    
    /**
     * Associate a teacher to a course.
     * 
     * @param courseId - the course id.
     * @param teacherId - the teacher id.
     * @return 1 if insert successful else 0.
     */

    int insertAssociation(String courseId, String teacherId) {

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;

        int success = 0;
        String activeStatus = "Yes";

        try {
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);

            //sql statement
            String insertAssociation = "INSERT IGNORE INTO coursesteaching (CourseId, TeacherId) VALUES (" + "'" + courseId + "'," + "'" + teacherId + "')";

            myStatement = myConnection.createStatement();
            success = myStatement.executeUpdate(insertAssociation);

            String activeTeacher = "UPDATE IGNORE teachers SET Available =" + "'" + activeStatus + "'" + "WHERE TeacherId='" + teacherId + "'";

            myStatement = myConnection.createStatement();
            success = myStatement.executeUpdate(activeTeacher);

            String activeCourse = "UPDATE IGNORE courses SET Available =" + "'" + activeStatus + "'" + "WHERE Id ='" + courseId + "'";

            myStatement = myConnection.createStatement();
            success = myStatement.executeUpdate(activeCourse);

        } catch (SQLException ex) {
            Logger.getLogger(OperationsDbUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }
        return success;
    }
    
    /**
     * Delete a course.
     * 
     * @param courseId - id of the course we want to delete.
     * @return 1 if delete successful else 0.
     */

    int deleteCourse(String courseId) {
        int success = 0;

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;

        try {
            String deletedStatus = "N/A";
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);

            //sql statement
            String deleteCourse = "UPDATE IGNORE courses SET Available =" + "'" + deletedStatus + "'" + "WHERE Id='" + courseId + "'";
            myStatement = myConnection.createStatement();
            success = myStatement.executeUpdate(deleteCourse);
        } catch (SQLException ex) {
            Logger.getLogger(OperationsDbUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }

        return success;
    }
    
    /**
     * Delete teacher.
     * @param teacherId - the teacher id.
     * @return 1 if delete successful else 0.
     */
    int deleteTeacher(String teacherId) {
        int success = 0;

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;

        try {
            String deletedStatus = "N/A";
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);

            //sql statement
            String deleteTeacher = "UPDATE teachers SET Available =" + "'" + deletedStatus + "'" + "WHERE TeacherId='" + teacherId + "'";
            myStatement = myConnection.createStatement();
            success = myStatement.executeUpdate(deleteTeacher);

        } catch (SQLException ex) {
            Logger.getLogger(OperationsDbUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }

        return success;
    }
    
    /**
     * Delete an association.
     * 
     * @param courseId - the course id.
     * @param teacherId - the teacher id.
     * @return 1 if delete successful else 0.
     */

    int deleteAssociation(String courseId, String teacherId) {
        int success = 0;

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;

        try {
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);

            //sql statement
            String deleteAssociation = "DELETE FROM coursesteaching WHERE CourseId='" + courseId + "' AND TeacherId='" + teacherId + "'";
            myStatement = myConnection.createStatement();
            success = myStatement.executeUpdate(deleteAssociation);

        } catch (SQLException ex) {
            Logger.getLogger(OperationsDbUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }

        return success;
    }
    
    /**
     * Reactivate a course or a teacher.
     * 
     * @param id - the course or teacher id we want to reactivate.
     * @param option - the thing we want to reactivate (course or teacher)
     * @return 1 if reactivate successful else 0.
     */

    int reactivateData(String id, String option) {
        int success = 0;

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;

        try {
            String activateStatus = "Yes";
            //get a connection
            myConnection = DriverManager.getConnection(url, user, psw);

            if (option.equals("course")) {
                //sql statement
                String reactiveCourse = "UPDATE IGNORE courses SET Available =" + "'" + activateStatus + "'" + "WHERE Id='" + id + "'";
                myStatement = myConnection.createStatement();
                success = myStatement.executeUpdate(reactiveCourse);
            } else {
                String reactiveTeacher = "UPDATE IGNORE teachers SET Available =" + "'" + activateStatus + "'" + "WHERE TeacherId='" + id + "'";
                myStatement = myConnection.createStatement();
                success = myStatement.executeUpdate(reactiveTeacher);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OperationsDbUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //close jdbc obj
            close(myConnection, myStatement, myResultSet);
        }

        return success;
    }
    
    /**
     * Closing DB connection.
     * 
     * @param myConnection
     * @param myStatement
     * @param myResultSet 
     */

    private void close(Connection myConnection, Statement myStatement, ResultSet myResultSet) {
        try {
            if (myResultSet != null) {
                myResultSet.close();
            }
            if (myStatement != null) {
                myStatement.close();
            }
            // non chiude la connessione con il db, più o meno la rende disponibile ad un altra op sul db
            if (myConnection != null) {
                myConnection.close();
            }
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }
}
