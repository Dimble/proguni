package com;


/**
 * @author Fortunato Cogliandro
 */
public class Teaching {

    private int id;
    private int teacherId;
    private String courseId = null;

    public Teaching(int id, int teacherId, String courseId) {
        this.id = id;
        this.teacherId = teacherId;
        this.courseId = courseId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    @Override
    public String toString() {
        return "Teaching{" + "id=" + id + ", teacherId=" + teacherId + ", courseId=" + courseId + '}';
    }

}
