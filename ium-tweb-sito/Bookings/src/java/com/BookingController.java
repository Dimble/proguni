package com;

import com.google.gson.Gson;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Fortunato Cogliandro
 */
//@WebServlet(urlPatterns = {"/BookingController"})
public class BookingController extends HttpServlet {

    private OperationsDbUtil operationsDbUtil = null;
    private Gson gson = new Gson();
    String requestedAction = null;
    //@Resource(name = "jdbc/bookingsDB")
    //private DataSource dataSource;

    // method started at opening to init everything
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        ServletContext ctx = servletConfig.getServletContext();
        String url = ctx.getInitParameter("DB-URL");
        String user = ctx.getInitParameter("username");
        operationsDbUtil = new OperationsDbUtil(url, user);
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        HttpSession session = request.getSession(false);

        if (session == null || session.getAttribute("loggedUser") == null) {
            response.sendRedirect("homepage.jsp"); // No logged-in user found, so redirect to login page.
        } else {
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
            response.setDateHeader("Expires", 0);
            chain.doFilter(req, res);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            //processRequest(request, response);
            // list the catalog with MVC

            // read the command parameter
            requestedAction = request.getParameter("action");

            // if action is missing, route to the default page
            if (requestedAction == null) {
                requestedAction = "homepage";
            }

            //route to the appropriate method
            switch (requestedAction) {
                case "homepage":
                    RequestDispatcher dispatcher = request.getRequestDispatcher("/homepage.jsp");
                    dispatcher.forward(request, response);
                    break;
                case "catalog_page":
                    dispatcher = request.getRequestDispatcher("/catalog.jsp");
                    dispatcher.forward(request, response);
                    break;
                case "show_catalog":
                    showCatalog(request, response);
                    break;
                case "show_courses":
                    showCourses(request, response);
                    break;
                case "show_teachers":
                    showTeachers(request, response);
                    break;
                case "show_available_hours":
                    showAvailableHours(request, response);
                    break;
                case "user_reservations_page":
                    dispatcher = request.getRequestDispatcher("/user-reservations.jsp");
                    dispatcher.forward(request, response);
                    break;
                case "user_reservations":
                    showUserReservations(request, response);
                    break;
                case "delete_reservation":
                    deleteUserReservation(request, response);
                    break;
                case "book":
                    dispatcher = request.getRequestDispatcher("/booking-lesson-form.jsp");
                    dispatcher.forward(request, response);
                    break;
                case "admin_operations":
                    dispatcher = request.getRequestDispatcher("/admin-operations.jsp");
                    dispatcher.forward(request, response);
                    break;
                case "logout":
                    logout(request, response);
                    break;
                case "history_page":
                    dispatcher = request.getRequestDispatcher("/bookingHistory.jsp");
                    dispatcher.forward(request, response);
                    break;
                case "booking_history":
                    bookingHistory(request, response);
                    break;
                default:
                    dispatcher = request.getRequestDispatcher("/homepage.jsp");
                    dispatcher.forward(request, response);
                    break;
            }

        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            // read the command parameter
            requestedAction = request.getParameter("action");

            // if action is missing, route to the default page
            if (requestedAction == null) {
                requestedAction = "?";
            }

            //route to the appropriate method
            switch (requestedAction) {
                case "homepage":
                    RequestDispatcher dispatcher = request.getRequestDispatcher("/homepage.jsp");
                    dispatcher.forward(request, response);
                case "book_lesson":
                    bookLesson(request, response);
                    break;
                case "login_request":
                    validateUser(request, response);
                    break;
                case "insert_course":
                    insertCourse(request, response);
                    break;
                case "reactivate_data":
                    reactivateData(request, response);
                    break;
                case "insert_teacher":
                    insertTeacher(request, response);
                    break;
                case "insert_association":
                    insertAssociation(request, response);
                    break;
                case "delete_course":
                    deleteCourse(request, response);
                    break;
                case "delete_teacher":
                    deleteTeacher(request, response);
                    break;
                case "delete_association":
                    deleteAssociation(request, response);
                    break;
            }
        } catch (SQLException ex) {
            Logger.getLogger(BookingController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * Show all available courses.
     *
     * @param request
     * @param response
     * @throws SQLException
     * @throws IOException
     */
    private void showCatalog(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        // get catalog from db
        List<Operation> lessonsCatalog = operationsDbUtil.getAvailablePrivateLessons();

        response.getWriter().write(gson.toJson(lessonsCatalog));
    }

    /**
     * Show courses.
     *
     * @param request
     * @param response
     * @throws SQLException
     * @throws IOException
     */
    private void showCourses(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        String option = null;
        List<Course> coursesList = null;

        if (request.getParameter("option") != null) {
            option = request.getParameter("option");

            // get courses list
            coursesList = operationsDbUtil.getCourses(option);
        }

        response.getWriter().write(gson.toJson(coursesList));
    }

    /**
     * Show teachers.
     *
     * @param request
     * @param response
     * @throws SQLException
     * @throws IOException
     */
    private void showTeachers(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        String course = null;
        String courseId = null;
        String teacherId = null;
        String option = null;

        List<Teacher> teachersList = null;

        if (request.getParameter("option") != null) {
            course = request.getParameter("course");
            courseId = request.getParameter("courseId");
            option = request.getParameter("option");

            // get teachers list
            teachersList = operationsDbUtil.getTeachers(course, courseId, option);
        }
        response.getWriter().write(gson.toJson(teachersList));
    }

    /**
     * Show user reservations.
     *
     * @param request
     * @param response
     * @throws IOException
     * @throws SQLException
     * @throws ServletException
     */
    private void showUserReservations(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException, ServletException {
        String username = null;

        HttpSession session = request.getSession();

        if (session.getAttribute("loggedUser") != null) {
            username = (String) session.getAttribute("loggedUser");
        } else if (request.getParameter("android_request") != null) {
            username = (String) request.getParameter("android_request");
        }

        // get catalog from db
        List<Operation> userReservations = operationsDbUtil.getUserReservations(username);
        if (userReservations != null) {
            request.setAttribute("AVAILABLE_COURSES", userReservations);
            response.getWriter().write(gson.toJson(userReservations));
        }
    }

    /**
     * Delete user reservation.
     *
     * @param request
     * @param response
     * @throws IOException
     * @throws SQLException
     * @throws ServletException
     */
    private void deleteUserReservation(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException, ServletException {
        //read reservation id
        int reservationId = Integer.parseInt(request.getParameter("reservationId"));
        // get reservation from database & delete it
        operationsDbUtil.deleteReservation(reservationId);

        // send back to the main page
        RequestDispatcher dispatcher = request.getRequestDispatcher("/user-reservations.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * This method is used to show available hours for lesson booking.
     *
     * @param request
     * @param response
     * @throws IOException
     * @throws SQLException
     */
    private void showAvailableHours(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {
        String teacherName = null;
        String day = null;

        String[] availableHours = {"15:00-16:00", "16:00-17:00", "17:00-18:00", "18:00-19:00"};
        List<String> hoursList = new ArrayList<>();
        hoursList.addAll(Arrays.asList(availableHours));

        if (request.getParameter("teacherName") != null && request.getParameter("day") != null) {
            teacherName = request.getParameter("teacherName");
            day = request.getParameter("day");
        }

        List<String> notAvailableHoursList = operationsDbUtil.getAvailableHours(teacherName, day);
        hoursList.removeAll(notAvailableHoursList);
        response.getWriter().write(gson.toJson(hoursList));
    }

    /**
     * User login handling and session tracking.
     *
     *
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     * @throws SQLException
     */
    private void validateUser(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {

        String username = null;
        String password = null;
        String role = null;
        int isValid = 0;

        if (request.getParameter("username") != null && request.getParameter("password") != null) {
            username = request.getParameter("username");
            password = request.getParameter("password");
            role = operationsDbUtil.validateUser(username, password);
        }

        if (role != null) {
            HttpSession session = request.getSession();
            session.setAttribute("loggedUser", username);
            session.setAttribute("role", role);
            isValid = 1;
        }

        response.getWriter().write(gson.toJson(isValid));
    }

    /**
     * Logout method.
     *
     * @param request
     * @param response
     * @throws IOException
     */
    private void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        session.removeAttribute("loggedUser");
        session.removeAttribute("role");
        session.invalidate();

        response.getWriter().write(gson.toJson("logout"));
    }

    /**
     * This method is used to show all the booking history. (Admin operation)
     *
     * @param request
     * @param response
     * @throws IOException
     * @throws SQLException
     * @throws ServletException
     */
    private void bookingHistory(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException, ServletException {

        // get history from db
        List<Operation> bookingHistory = operationsDbUtil.getbookingHistory();
        response.getWriter().write(gson.toJson(bookingHistory));
    }

    /**
     * Method to book a lesson.
     *
     * @param request
     * @param response
     * @throws IOException
     * @throws SQLException
     */
    private void bookLesson(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException {

        String username = null;
        String day = null;
        String hour = null;
        String course = null;
        String teacher = null;
        String status = null;
        int insert = 0;

        if (request.getParameter("username") != null && request.getParameter("day") != null && request.getParameter("hour") != null && request.getParameter("course") != null && request.getParameter("teacher") != null && request.getParameter("status") != null) {
            username = request.getParameter("username");
            day = request.getParameter("day");
            hour = request.getParameter("hour");
            course = request.getParameter("course");
            teacher = request.getParameter("teacher");
            status = request.getParameter("status");

            insert = operationsDbUtil.bookLesson(username, day, hour, course, teacher, status);

            response.getWriter().write(gson.toJson(insert));
        } else {
            response.getWriter().write(gson.toJson(insert));
        }

    }

    // Admin operations for course / teacher and association handling.
    private void insertCourse(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int insert = 0;
        String courseId = null;
        String courseName = null;
        String available = "N/A";

        if (request.getParameter("courseId") != null && request.getParameter("courseName") != null) {
            courseId = request.getParameter("courseId");
            courseName = request.getParameter("courseName");

            insert = operationsDbUtil.insertCourse(courseId, courseName, available);
        }

        response.getWriter().write(gson.toJson(insert));
    }

    private void insertTeacher(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int insert = 0;
        String teacherId = null;
        String teacherName = null;
        String available = "N/A";

        if (request.getParameter("teacherId") != null && request.getParameter("teacherName") != null) {
            teacherId = request.getParameter("teacherId");
            teacherName = request.getParameter("teacherName");

            insert = operationsDbUtil.insertTeacher(teacherId, teacherName, available);
        }

        response.getWriter().write(gson.toJson(insert));
    }

    private void insertAssociation(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int insert = 0;
        String courseId = null;
        String teacherId = null;

        if (request.getParameter("courseId") != null && request.getParameter("teacherId") != null) {
            courseId = request.getParameter("courseId");
            teacherId = request.getParameter("teacherId");

            insert = operationsDbUtil.insertAssociation(courseId, teacherId);
        }

        response.getWriter().write(gson.toJson(insert));
    }

    private void deleteCourse(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String courseId = null;
        int deletion = 0;

        if (request.getParameter("courseId") != null) {
            courseId = request.getParameter("courseId");

            // get course from database & delete it
            deletion = operationsDbUtil.deleteCourse(courseId);
        }
        // send back datas
        response.getWriter().write(gson.toJson(deletion));
    }

    private void deleteTeacher(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String teacherId = null;
        int deletion = 0;

        if (request.getParameter("teacherId") != null) {
            teacherId = request.getParameter("teacherId");

            // get course from database & delete it
            deletion = operationsDbUtil.deleteTeacher(teacherId);
        }
        // send back datas
        response.getWriter().write(gson.toJson(deletion));
    }

    private void reactivateData(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = null;
        String option = null;
        int reactivation = 0;

        if (request.getParameter("option") != null) {
            id = request.getParameter("id");
            option = request.getParameter("option");

            // get course from database & delete it
            reactivation = operationsDbUtil.reactivateData(id, option);
        }
        // send back datas
        response.getWriter().write(gson.toJson(reactivation));
    }

    private void deleteAssociation(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String courseId = null;
        String teacherId = null;
        int deletion = 0;

        if (request.getParameter("courseId") != null && request.getParameter("teacherId") != null) {
            courseId = request.getParameter("courseId");
            teacherId = request.getParameter("teacherId");

            // get course from database & delete it
            deletion = operationsDbUtil.deleteAssociation(courseId, teacherId);
        }
        // send back datas
        response.getWriter().write(gson.toJson(deletion));
    }
}
