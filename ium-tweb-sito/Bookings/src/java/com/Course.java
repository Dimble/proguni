package com;

/**
 * @author Fortunato Cogliandro
 */
public class Course {

    private String id = null;
    private String courseName = null;
    private String available = null;

    public Course(String courseId, String name, String available) {
        this.id = courseId;
        this.courseName = name;
        this.available = available;
    }
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "Course{" + "id=" + id + ", courseName=" + courseName + ", available=" + available + '}';
    }
}
