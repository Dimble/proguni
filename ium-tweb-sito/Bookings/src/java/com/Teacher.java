package com;

/**
 * @author Fortunato Cogliandro
 */
public class Teacher {

    private String teacherId = null;
    private String firstNameLastName = null;
    private String available = null;

    public Teacher(String teacherId, String fullName, String available) {
        this.teacherId = teacherId;
        this.firstNameLastName = fullName;
        this.available = available;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getFirstNameLastName() {
        return firstNameLastName;
    }

    public void setFirstNameLastName(String firstNameLastName) {
        this.firstNameLastName = firstNameLastName;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "Teacher{" + "teacherId=" + teacherId + ", firstNameLastName=" + firstNameLastName + ", available=" + available + '}';
    }

}
