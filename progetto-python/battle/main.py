from classes.game import Person, bcolours
from classes.magic import Spell
from classes.inventary import Item
import random

# Create Black Magic
fire = Spell("Fire Bolt", 25, 600, "black")
thunder = Spell("Thunder Storm", 25, 600, "black")
blizzard = Spell("Frost Nova", 25, 600, "black")
meteor = Spell("Shadow Fury", 40, 1200, "black")
quake = Spell("Hurricane", 14, 140, "black")

# Create white magic
cure = Spell("Flash Heal", 25, 620, "white")
cura = Spell("Prayer of Mending", 32, 1500, "white")
curaga = Spell("Reborn", 50, 6000, "white")

# Create some items
potion = Item("Potion", "potion", "Heals 50HP", 50)
hipotion = Item("Hi-Potion", "potion", "Heals 100HP", 100)
superpotion = Item("Super Potion", "potion", "Heals 500 HP", 500)
elixer = Item("Elixer", "elixer", "Fully restores HP/MP of one party member", 9999)
hielixer = Item("MegaElixer", "elixer", "Fully restores party's HP/MP", 9999)

grenade = Item("Grenade", "attack", "Deals 500 damage", 500)

player_spells = [fire, thunder, blizzard, meteor, cure, cura]
enemy_spells = [fire, meteor, curaga]
player_items = [{"item": potion, "quantity": 15}, {"item": hipotion, "quantity": 5},
                {"item": superpotion, "quantity": 5}, {"item": elixer, "quantity": 5},
                {"item": hielixer, "quantity": 2}, {"item": grenade, "quantity": 5}]

# Instantiate People name, hp, mp, atk, df, magic, items
player1 = Person("Tidus :", 3260, 132, 250, 34, player_spells, player_items)
player2 = Person("Auron :", 4160, 188, 350, 34, player_spells, player_items)
player3 = Person("Yuna  :", 3089, 174, 268, 34, player_spells, player_items)

enemy1 = Person("Floating Death ", 1550, 130, 460, 325, enemy_spells, [])
enemy2 = Person("Seymour Beta   ", 5100, 601, 985, 25, enemy_spells, [])
enemy3 = Person("Floating Death ", 1550, 130, 460, 325, enemy_spells, [])

players = [player1, player2, player3]
enemies = [enemy1, enemy2, enemy3]

defeated_enemies = 0
defeated_players = 0

running = True
i = 0

print(bcolours.FAIL + bcolours.BOLD + "AN ENEMY ATTACKS!" + bcolours.ENDC)

while running:
    print("============================")

    print("NAME               HP                                  MP")
    for player in players:
        player.get_stats()

    print("\n")
    for enemy in enemies:
        enemy.get_enemy_stats()

    for player in players:

        if defeated_enemies != 3:

            player.choose_action()

            choice = input("    Choose action:")

            while choice == '' or int(choice) < 1 or int(choice) > 3:
                print("    " + bcolours.WARNING + "⚠⚠⚠ The choice must be between 1 and 3. Retry. ⚠⚠⚠" + bcolours.ENDC)
                choice = input("    Choose action:")
            index = int(choice) - 1

            if index == 0:
                dmg = player.generate_damage()
                enemy = player.choose_target(enemies)
                enemies[enemy].take_damage(dmg)
                print(bcolours.WARNING + "⚔⚔⚔ " + player.name.replace(" ", "").replace(":", "") + " attacked " + enemies[enemy].name.replace(" ", "").replace(":", "") +
                      " for", dmg, "points of damage ⚔⚔⚔" + bcolours.ENDC)

                if enemies[enemy].get_hp() == 0:
                    print(bcolours.OKGREEN + bcolours.BOLD + "💀💀 " + enemies[enemy].name.replace(" ", "").replace(":", "") + " has died! 💀💀" + bcolours.ENDC)
                    del enemies[enemy]
                    defeated_enemies += 1

            elif index == 1:
                player.choose_magic()
                magic_choice = input("    Choose magic:")

                while magic_choice == '' or int(magic_choice) < 0 or int(magic_choice) > 6:
                    print("    " + bcolours.WARNING + "⚠⚠⚠ The choice must be between 1 and 6. Retry. ⚠⚠⚠" + bcolours.ENDC)
                    magic_choice = input("    Choose magic:")

                if magic_choice == -1:
                    continue

                spell = player.magic[int(magic_choice)-1]
                magic_dmg = spell.generate_damage()

                current_mp = player.get_mp()
                if spell.cost > current_mp:
                    print(bcolours.FAIL + "\nNot enough MP\n" + bcolours.ENDC)
                    continue

                player.reduce_mp(spell.cost)

                if spell.type == "white":
                    player.heal(magic_dmg)
                    print(bcolours.OKBLUE + "\n" + "♥♥♥ " + spell.name + " heals for", str(magic_dmg), "HP ♥♥♥" + bcolours.ENDC)
                elif spell.type == "black":
                    enemy = player.choose_target(enemies)
                    enemies[enemy].take_damage(magic_dmg)
                    print(bcolours.OKBLUE + "\n" + "*’⛧`* " + spell.name + " deals", str(magic_dmg), "points of damage to " +
                          enemies[enemy].name.replace(" ", "").replace(":", "") + bcolours.ENDC)

                    if enemies[enemy].get_hp() == 0:
                        print(bcolours.OKGREEN + bcolours.BOLD + "💀💀 " + enemies[enemy].name.replace(" ", "").replace(":", "") +
                              " has died! 💀💀" + bcolours.ENDC)
                        del enemies[enemy]
                        defeated_enemies += 1

            elif index == 2:
                player.choose_item()
                item_choice = input("    Choose item:")

                while item_choice == '' or int(item_choice) < 0 or int(item_choice) > 6:
                    print("    " + bcolours.WARNING + "⚠⚠⚠ The choice must be between 1 and 6. Retry. ⚠⚠⚠" + bcolours.ENDC)
                    item_choice = input("    Choose magic:")

                if item_choice == -1:
                    continue

                item = player.items[int(item_choice) -1]["item"]

                if player.items[int(item_choice)-1]["quantity"] == 0:
                    print(bcolours.FAIL + "\n" + "None left..." + bcolours.ENDC)
                    continue

                player.items[int(item_choice)-1]["quantity"] -= 1

                if item.type == "potion":
                    player.heal(item.prop)
                    print(bcolours.OKGREEN + "\n" + "♥♥♥ " + item.name + " heals for", str(item.prop), " HP ♥♥♥" + bcolours.ENDC)
                elif item.type == "elixer":
                    if item.name == "MegaElixer":
                        for i in players:
                            i.hp = i.maxhp
                            i.mp = i.maxmp
                    else:
                        player.hp = player.maxhp
                        player.mp = player.maxmp
                    print(bcolours.OKGREEN + "\n" + "♥♥♥ " + item.name + " fully restores HP/MP ♥♥♥" + bcolours.ENDC)
                elif item.type == "attack":
                    enemy = player.choose_target(enemies)
                    enemies[enemy].take_damage(item.prop)
                    print(bcolours.FAIL + "\n" + item.name + " deals", str(item.prop), " points of damage to " +
                          enemies[enemy].name + bcolours.ENDC)

                    if enemies[enemy].get_hp() == 0:
                        print(bcolours.OKGREEN + bcolours.BOLD + "💀💀 " + enemies[enemy].name.replace(" ", "").replace(":", "") +
                              " has died! 💀💀" + bcolours.ENDC)
                        del enemies[enemy]
                        defeated_enemies += 1

    # Check if player has won
    if defeated_enemies == 3:
        print(bcolours.BOLD + bcolours.OKGREEN + "\nYOU WIN!" + bcolours.ENDC)
        running = False

    print("\n")

    # Enemy attack phase
    for enemy in enemies:

        if defeated_players != 3:
            enemy_choice = random.randrange(0, 2)
            # normal atk
            if enemy_choice == 0:
                # Choose attack
                target = random.randrange(0, len(players))
                enemy_dmg = enemy.generate_damage()

                players[target].take_damage(enemy_dmg)
                print(bcolours.FAIL + bcolours.BOLD + "⚔⚔⚔ " + enemy.name.replace(" ", "").replace(":", "") + " attacks " +
                      players[target].name.replace(" ", "").replace(":", "") + " for", enemy_dmg, " ⚔⚔⚔", bcolours.ENDC)

                if players[target].get_hp() == 0:
                    print(bcolours.WARNING + bcolours.BOLD + "💀💀 " + players[target].name.replace(" ", "").replace(":", "") +
                          " has died! 💀💀" + bcolours.ENDC)
                    del players[target]
                    defeated_players += 1
            # magics
            elif enemy_choice == 1:
                spell, magic_dmg = enemy.choose_enemy_spell()
                magic_dmg = spell.generate_damage()
                enemy.reduce_mp(spell.cost)

                if spell.type == "white":
                    enemy.heal(magic_dmg)
                    print(bcolours.FAIL + "♥♥♥ " + spell.name + " heals " + enemy.name.replace(" ", "").replace(":", "") +
                          " for", str(magic_dmg), "HP ♥♥♥" + bcolours.ENDC)
                elif spell.type == "black":
                    if len(players) != 0:
                        target = random.randrange(0, len(players))
                    players[target].take_damage(magic_dmg)
                    print(bcolours.FAIL + bcolours.BOLD + "*’⛧`* " + enemy.name.replace(" ", "").replace(":","") + "'s " + spell.name +
                          " deals", str(magic_dmg), "points of damage to " +
                          players[target].name.replace(" ", "").replace(":","") + bcolours.ENDC)

                    if players[target].get_hp() == 0:
                        print(bcolours.WARNING + bcolours.BOLD + "💀💀 " + players[target].name.replace(" ", "").replace(":", "") +
                              " has died! 💀💀" + bcolours.ENDC)
                        del players[target]
                        defeated_players += 1

    # Check if Enemy won
    if defeated_players == 3:
        print(bcolours.BOLD + bcolours.FAIL + "\n💀💀 Your enemies have defeated you! 💀💀" + bcolours.ENDC)
        running = False

