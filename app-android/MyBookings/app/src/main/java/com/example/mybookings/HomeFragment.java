package com.example.mybookings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.mybookings.utils.PreferenceUtils;

public class HomeFragment extends Fragment {

    private View view = null;
    private FloatingActionButton floatingActionButton = null;
    private PreferenceUtils utils = null;
    private Button btnProva = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);
        utils = new PreferenceUtils();

        floatingActionButton = view.findViewById(R.id.btnBooking);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bookingFormIntent = new Intent(getContext(), com.example.mybookings.BookingForm.class);
                if (utils.getUsername(getContext()) != null) {
                    bookingFormIntent.putExtra("loggedUser", utils.getUsername(getContext()));
                    startActivity(bookingFormIntent);
                } else {
                    Toast.makeText(getContext(), "Errore - utente non valido", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }
}
