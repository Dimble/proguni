package com.example.mybookings;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mybookings.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginActivity extends AppCompatActivity {

    EditText etUsername = null;
    EditText etPassword = null;
    Button btnLogin = null;
    private String strUrl = null;
    private String username = null;
    private String password = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etUsername.getText().toString().trim().isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Errore: il campo username non può essere vuoto.", Toast.LENGTH_SHORT).show();
                } else if (etPassword.getText().toString().trim().isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Errore: il campo password non può essere vuoto.", Toast.LENGTH_SHORT).show();
                } else if (etUsername.getText().toString().trim().isEmpty() || etPassword.getText().toString().trim().isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Per favore, inserire username e password.", Toast.LENGTH_SHORT).show();
                }else {
                    username = etUsername.getText().toString().trim();
                    password = etPassword.getText().toString().trim();

                    strUrl = "http://192.168.1.3:8080/Bookings/BookingController?action=login_request&username=" + username + "&password=" + password;
                    new LogUserTask().execute();
                }
            }
        });
    }

    public class LogUserTask extends AsyncTask<String, String, String> {
        private int userExists = -1;
        //if i want to do something before sending data
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        // received data
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            verifyUser(userExists);
        }

        // send data to server
        @Override
        protected String doInBackground(String... strings) {
            try {
                URL url = new URL(strUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.connect();

                // reading received data
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String value = bufferedReader.readLine();
                userExists = Integer.parseInt(value);

            } catch (Exception e) {
                System.out.println(e);
            }
            return null;
        }
    }

    private void initViews(){
        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);

        PreferenceUtils utils = new PreferenceUtils();

        if (utils.getUsername(this) != null){
            Intent intent = new Intent(LoginActivity.this, LoggedUser.class);
            startActivity(intent);
        }else{
            Toast.makeText(this, "Utente disconnesso.", Toast.LENGTH_SHORT).show();
        }
    }

    private void verifyUser(int userExists){
        if (userExists == 1) {
            Toast.makeText(LoginActivity.this, "Connessione effettuata con successo!.", Toast.LENGTH_SHORT).show();
            PreferenceUtils.saveUsername(username, this);
            PreferenceUtils.savePassword(password, this);
            Intent accountsIntent = new Intent(LoginActivity.this, com.example.mybookings.LoggedUser.class);
            accountsIntent.putExtra("loggedUser", etUsername.getText().toString().trim());
            emptyInputEditText();
            startActivity(accountsIntent);
            finish();
        } else {
            Toast.makeText(LoginActivity.this, "username o password errati. ", Toast.LENGTH_SHORT).show();
        }
    }

    private void emptyInputEditText(){
        etUsername.setText(null);
        etPassword.setText(null);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
