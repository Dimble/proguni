package com.example.mybookings;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class BookingDeleted extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_deleted);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(BookingDeleted.this, LoggedUser.class);
        startActivity(i);
    }
}
