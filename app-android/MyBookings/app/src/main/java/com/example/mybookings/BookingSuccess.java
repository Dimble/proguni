package com.example.mybookings;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class BookingSuccess extends AppCompatActivity {

    private HomeFragment homeFragment = new HomeFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_success);

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(BookingSuccess.this, LoggedUser.class);
        startActivity(i);
    }
}
