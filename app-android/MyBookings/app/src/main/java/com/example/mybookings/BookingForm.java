package com.example.mybookings;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static android.R.layout.simple_spinner_item;

public class BookingForm extends AppCompatActivity {

    private ArrayList<String> coursesList;
    private ArrayList<String> teachersList;
    private ArrayList<String> hoursList;
    private ArrayList<Operation> jsonList;
    private ArrayAdapter<String> coursesAdapter;
    private ArrayAdapter<String> teachersAdapter;
    private ArrayAdapter<String> hoursAdapter;
    private Button bookingBtn = null;
    private int booked = 0;
    private JSONArray jsonArray = null;
    private Spinner courseSpinner = null;
    private Spinner teacherSpinner = null;
    private Spinner daysSpinner = null;
    private Spinner hoursSpinner = null;
    private String strUrl = null;
    private String spinnerAction = "course";
    private String jsonString = null;
    private String courseName = null;
    private String teacherDatas = null;
    private String selectedDay = null;
    private String selectedHour = null;
    private String nameFromIntent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_form);

        courseSpinner = findViewById(R.id.course_spinner);
        teacherSpinner = findViewById(R.id.teachers_spinner);
        daysSpinner = findViewById(R.id.days_spinner);
        hoursSpinner = findViewById(R.id.hours_spinner);
        bookingBtn = findViewById(R.id.btnBookingForm);

        Intent intent = getIntent();
        if (intent.hasExtra("loggedUser")) {
             nameFromIntent = getIntent().getStringExtra("loggedUser");
        }

        new FillSpinnerTask().execute();

        // filling spinner with teachers
        courseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                courseName = parent.getItemAtPosition(position).toString();
                spinnerAction = "teachers";
                new FillSpinnerTask().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // filling spinner with days
        teacherSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                daysSpinner.setSelection(0);
                hoursSpinner.setSelection(0);

                if(teacherSpinner.getSelectedItem() == "Insegnante"){

                } else {
                    // selected teacher
                    teacherDatas = parent.getItemAtPosition(position).toString();

                    // Create an ArrayAdapter using the string array and a default spinner layout
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(BookingForm.this,
                            R.array.days_array, R.layout.spinner_style);
                    // Specify the layout to use when the list of choices appears
                    adapter.setDropDownViewResource(R.layout.spinner_dropdown);
                    // Apply the adapter to the spinner
                    daysSpinner.setAdapter(adapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // filling spinner with hours

        daysSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedDay = parent.getItemAtPosition(position).toString();
                    spinnerAction = "hours";
                    new FillSpinnerTask().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        hoursSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedHour = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bookingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinnerAction = "book";
                new BookingTask().execute();
            }
        });


    }

    public class FillSpinnerTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            if (spinnerAction.equals("course"))
                strUrl = "http://192.168.1.3:8080/Bookings/BookingController?action=show_courses&option=association";
            else if (spinnerAction.equals("teachers") && courseName != null)
                strUrl = "http://192.168.1.3:8080/Bookings/BookingController?action=show_teachers&option=form&course=" + courseName.toString();
            else if (spinnerAction.equals("hours") && selectedDay != null && teacherDatas != null)
                strUrl = "http://192.168.1.3:8080/Bookings/BookingController?action=show_available_hours&teacherName=" + teacherDatas
                        + "&day=" + selectedDay;
            else if (spinnerAction.equals("book") && courseName != null && teacherDatas != null && selectedDay != null && selectedHour != null)
                strUrl = "http://192.168.1.3:8080/Bookings/BookingController?action=book_lesson&username=mario.rossi" +
                        "&day=" + selectedDay + "&hour=" + selectedHour + "&course=" + courseName + "&teacher=" + teacherDatas + "&status=Attiva";
            else
                Toast.makeText(BookingForm.this, "onPreExecute Error", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null && spinnerAction.equals("course"))
                getCourses(result);
            else if (result != null && spinnerAction.equals("teachers"))
                getTeachers(result);
            else if (result != null && spinnerAction.equals("hours"))
                getAvailableHours(result);
            else
                Toast.makeText(BookingForm.this, "Errore: json vuoto." + courseName, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                URL url = new URL(strUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();

                while ((jsonString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(jsonString + "\n");
                }

                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();

                return stringBuilder.toString().trim();


            } catch (IOException exception) {
                System.out.println(exception.getMessage());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    public class BookingTask extends AsyncTask<Void, Void, Integer> {
        @Override
        protected void onPreExecute() {
            if (spinnerAction.equals("book") && courseName != null && teacherDatas != null && selectedDay != null && selectedHour != null)
                strUrl = "http://192.168.1.3:8080/Bookings/BookingController?action=book_lesson&username=" + nameFromIntent +
                        "&day=" + selectedDay + "&hour=" + selectedHour + "&course=" + courseName + "&teacher=" + teacherDatas + "&status=Attiva";
            else
                Toast.makeText(BookingForm.this, "[Errore] Prenotazione fallita, ricontrollare i campi.", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(Integer result) {
            verifyBooking(result);
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            try {
                URL url = new URL(strUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.connect();

                // reading received data
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String value = bufferedReader.readLine();
                booked = Integer.parseInt(value);

                return booked;
            } catch (IOException exception) {
                System.out.println(exception.getMessage());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    public void getCourses(String myJson) {
        jsonList = new ArrayList<>();
        coursesList = new ArrayList<>();
        try {
            int count = 0;
            jsonArray = new JSONArray(myJson);
            String courseId = null;
            String courseName = null;

            while (count < jsonArray.length()) {
                JSONObject oneObject = jsonArray.getJSONObject(count);
                // Pulling items from the array
                courseId = oneObject.getString("id");
                courseName = oneObject.getString("courseName");

                Operation allCoursesDatas = new Operation(courseId, courseName, null);
                jsonList.add(allCoursesDatas);  //qua ho i json con i dati del corso

                coursesList.add(jsonList.get(count).getCourseName().toString());

                count++;
            }

            coursesAdapter = new ArrayAdapter<>(BookingForm.this, R.layout.spinner_style ,coursesList);
            coursesAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
            courseSpinner.setAdapter(coursesAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getTeachers(String myJson) {
        jsonList.clear();
        teachersList = new ArrayList<>();
        try {
            int count = 0;
            jsonArray = new JSONArray(myJson);
            String teacherDatas = null;

            while (count < jsonArray.length()) {
                JSONObject oneObject = jsonArray.getJSONObject(count);
                // Pulling items from the array
                teacherDatas = oneObject.getString("firstNameLastName");

                Operation allTeachersDatas = new Operation(null, null, null, teacherDatas);
                jsonList.add(allTeachersDatas);  //qua ho i json con i dati del corso
                teachersList.add(jsonList.get(count).getTeacherDatas().toString());

                count++;
            }

            teachersAdapter = new ArrayAdapter<>(BookingForm.this, R.layout.spinner_style, teachersList);
            teachersAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
            teacherSpinner.setAdapter(teachersAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getAvailableHours(String myJson) {
        jsonList = new ArrayList<>();
        hoursList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(myJson);

            for (int i = 0; i < jsonArray.length(); i++) {
                hoursList.add(jsonArray.getString(i));
            }

            hoursAdapter = new ArrayAdapter<>(BookingForm.this, R.layout.spinner_style, hoursList);
            hoursAdapter.setDropDownViewResource(R.layout.spinner_dropdown);
            hoursSpinner.setAdapter(hoursAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void verifyBooking(Integer result) {
        if (result != 0) {
            Intent intent = new Intent(BookingForm.this, BookingSuccess.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Attenzione! Prenotazione fallita.", Toast.LENGTH_SHORT).show();
        }
    }
}
