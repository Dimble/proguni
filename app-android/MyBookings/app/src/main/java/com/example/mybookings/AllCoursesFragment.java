package com.example.mybookings;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.mybookings.utils.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AllCoursesFragment extends Fragment {

    private String strUrl = null;
    private String jsonString = null;
    private JSONArray jsonArray = null;
    private FloatingActionButton floatingActionButton = null;
    private PreferenceUtils utils = null;
    private AllCoursesListAdapter allCoursesListAdapter= null;
    View view = null;
    ListView listView = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_all_courses, container, false);
        listView = (ListView) view.findViewById(R.id.lvAllCourses);
        allCoursesListAdapter = new AllCoursesListAdapter(getActivity(), R.layout.adapter_view_layout_courses);
        listView.setAdapter(allCoursesListAdapter);
        new FillListTask().execute();

        utils = new PreferenceUtils();

        floatingActionButton = view.findViewById(R.id.btnAllCoursesBooking);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bookingFormIntent = new Intent(getContext(), com.example.mybookings.BookingForm.class);
                if (utils.getUsername(getContext()) != null) {
                    bookingFormIntent.putExtra("loggedUser", utils.getUsername(getContext()));
                    startActivity(bookingFormIntent);
                } else {
                    Toast.makeText(getContext(), "Errore - utente non valido", Toast.LENGTH_SHORT).show();
                }
            }
        });


        return view;
    }

    public class FillListTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            strUrl = "http://192.168.1.3:8080/Bookings/BookingController?action=show_catalog";
        }

        @Override
        protected void onPostExecute(String result) {
            if(result != null)
                    parseJsonData(result);
            else
                Toast.makeText(getContext(), "No json array", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {

                URL url = new URL(strUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();

                while ((jsonString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(jsonString + "\n");
                }

                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();

                return stringBuilder.toString().trim();


            } catch (IOException exception) {
                System.out.println(exception.getMessage());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    public void parseJsonData(String myJson){
        try {
            int count = 0;
            jsonArray = new JSONArray(myJson);
            String courseName = null;
            String teacherDatas = null;

            while (count < jsonArray.length()){
                JSONObject oneObject = jsonArray.getJSONObject(count);
                // Pulling items from the array
                courseName = oneObject.getString("courseName");
                teacherDatas = oneObject.getString("teacherDatas");

                Operation allCoursesDatas = new Operation(null, courseName, teacherDatas);
                allCoursesListAdapter.add(allCoursesDatas);
                count++;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
