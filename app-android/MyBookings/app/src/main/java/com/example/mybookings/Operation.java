package com.example.mybookings;

public class Operation {

    private int bookingId;
    private String courseId;
    private String studentDatas;
    private String courseName;
    private String day;
    private String hour;
    private String teacherDatas;
    private String status;

    public Operation(int bookingId, String courseName, String day, String hour, String teacherDatas, String status) {
        this.bookingId = bookingId;
        this.courseName = courseName;
        this.day = day;
        this.hour = hour;
        this.teacherDatas = teacherDatas;
        this.status = status;
    }

    public Operation(String courseId, String courseName, String teacherDatas) {
        if(courseId == null) {
            this.courseName = courseName;
            this.teacherDatas = teacherDatas;
        } else if(teacherDatas == null){
            this.courseId = courseId;
            this.courseName = courseName;
        }
    }

    public  Operation (String courseName, String day, String hour, String teacherDatas){
        if(teacherDatas != null)
            this.teacherDatas = teacherDatas;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    Operation(int bookingId, String studentDatas, String courseName, String day, String hour, String teacherDatas, String status) {
        this.bookingId = bookingId;
        this.studentDatas = studentDatas;
        this.courseName = courseName;
        this.day = day;
        this.hour = hour;
        this.teacherDatas = teacherDatas;
        this.status = status;
    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public String getStudentDatas() {
        return studentDatas;
    }

    public void setStudentDatas(String studentDatas) {
        this.studentDatas = studentDatas;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getTeacherDatas() {
        return teacherDatas;
    }

    public void setTeacherDatas(String teacherDatas) {
        this.teacherDatas = teacherDatas;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Operation{" + "bookingId=" + bookingId + ", studentDatas=" + studentDatas + ", courseName=" + courseName + ", day=" + day + ", hour=" + hour + ", teacherDatas=" + teacherDatas + ", status=" + status + '}';
    }

}

