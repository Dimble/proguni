package com.example.mybookings;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.mybookings.utils.PreferenceUtils;


public class LoggedUser extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private TextView tvUserName;
    private String nameFromIntent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logged_user);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new HomeFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_home);
        }

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        TextView txtProfileName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tvUserName);
        Intent intent = getIntent();
        if (intent.hasExtra("loggedUser")) {
            nameFromIntent = getIntent().getStringExtra("loggedUser");
            if (nameFromIntent.equals("mario.rossi"))
                txtProfileName.setText("Mario Rossi");
            else
                txtProfileName.setText("Giuseppe Verdi");
        } else {
            String username = PreferenceUtils.getUsername(this);
            if (username.equals("mario.rossi"))
                txtProfileName.setText("Mario Rossi");
            else
                txtProfileName.setText("Giuseppe Verdi");
        }

        Bundle bundle = new Bundle();
        bundle.putString("LOGGED_USER", nameFromIntent);
        // set Fragmentclass Arguments
        MyReservationsFragment myReservationsFragment = new MyReservationsFragment();
        myReservationsFragment.setArguments(bundle);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new HomeFragment()).commit();
                break;
            case R.id.nav_all_courses:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new AllCoursesFragment()).commit();
                break;
            case R.id.nav_my_bookings:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new MyReservationsFragment()).commit();
                break;
            case R.id.nav_logout:
                PreferenceUtils.savePassword(null, this);
                PreferenceUtils.saveUsername(null, this);
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            moveTaskToBack(true);
            super.onBackPressed();
        }
    }

    public static MyReservationsFragment createInstance(String loggedUser) {
        MyReservationsFragment myReservationsFragment = new MyReservationsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("loggedUser", loggedUser);
        myReservationsFragment.setArguments(bundle);
        return myReservationsFragment;
    }
}
