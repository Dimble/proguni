package com.example.mybookings;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AllCoursesListAdapter extends ArrayAdapter<Operation> {

    List list = new ArrayList();

    public AllCoursesListAdapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
    public void add(Operation object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Operation getItem(int position) {
        return (Operation) list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        OperationHolder operationHolder = null;
        if(row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.adapter_view_layout_courses, parent, false);
            operationHolder = new OperationHolder();
            operationHolder.tvAllCourses = row.findViewById(R.id.tvAllCoursesName);
            operationHolder.tvAllCoursesTeacher = row.findViewById(R.id.tvTeachersAllCourses);
            row.setTag(operationHolder);

        } else {
            operationHolder = (OperationHolder) row.getTag();
        }

        Operation operations = (Operation) this.getItem(position);
        operationHolder.tvAllCourses.setText(operations.getCourseName());
        operationHolder.tvAllCoursesTeacher.setText(operations.getTeacherDatas());

        return row;
    }

    static class OperationHolder {
        TextView tvAllCourses, tvAllCoursesTeacher;
    }
}
