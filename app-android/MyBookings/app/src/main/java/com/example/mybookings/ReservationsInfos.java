package com.example.mybookings;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.TextView;

public class ReservationsInfos extends AppCompatActivity {

    private TextView tvInfosCourse, tvInfosDay, tvInfosHour, tvInfosTeacher, tvInfosStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservations_infos);

        tvInfosCourse = findViewById(R.id.tvInfosCourse);
        tvInfosDay = findViewById(R.id.tvInfosDay);
        tvInfosHour = findViewById(R.id.tvInfosHour);
        tvInfosTeacher = findViewById(R.id.tvInfosTeacher);
        tvInfosStatus = findViewById(R.id.tvInfosStatus);


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        getWindow().setLayout((int)(width*.8), (int)(height*.6));

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        tvInfosCourse.setText("Corso: " + extras.getString("EXTRA_COURSE"));
        tvInfosDay.setText("Giorno: " + extras.getString("EXTRA_DAY"));
        tvInfosHour.setText("Ora: " + extras.getString("EXTRA_HOUR"));
        tvInfosTeacher.setText("Insegnante: " + extras.getString("EXTRA_TEACHER"));
        tvInfosStatus.setText("Stato: " + extras.getString("EXTRA_STATUS"));

    }
}
