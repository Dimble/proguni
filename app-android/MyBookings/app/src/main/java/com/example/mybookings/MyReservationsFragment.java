package com.example.mybookings;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import android.widget.Toast;

import com.example.mybookings.utils.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class MyReservationsFragment extends Fragment {

    private String strUrl = null;
    private String loggedUser = null;
    private String jsonString = null;
    private JSONArray jsonArray = null;
    private PreferenceUtils preferenceUtils = null;
    private ReservationsListAdapter reservationsListAdapter = null;
    private ImageView ivInfos = null;
    private int itemPoistion = 0;
    View view;
    ListView listView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_my_reservations, container, false);
        listView = (ListView) view.findViewById(R.id.lvUserReservations);
        reservationsListAdapter = new ReservationsListAdapter(getActivity(), R.layout.adapter_view_layout);
        listView.setAdapter(reservationsListAdapter);
        new FillListTask().execute();



        return view;
    }

    public class FillListTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            preferenceUtils = new PreferenceUtils();
            if (preferenceUtils.getUsername(getContext()) != null) {
                strUrl = "http://192.168.1.3:8080/Bookings/BookingController?action=user_reservations&android_request=" + preferenceUtils.getUsername(getContext());
            } else {
                Toast.makeText(getContext(), "Utente non riconosciuto", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null)
                parseJsonData(result);
            else
                Toast.makeText(getContext(), "No json array", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {

                URL url = new URL(strUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();

                while ((jsonString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(jsonString + "\n");
                }

                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();

                return stringBuilder.toString().trim();

            } catch (IOException exception) {
                System.out.println(exception.getMessage());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

    public void parseJsonData(String myJson) {
        try {
            jsonArray = new JSONArray(myJson);
            int count = 0;
            int bookingId = 0;
            String courseName = null;
            String day = null;
            String hour = null;
            String teacherDatas = null;
            String status = null;

            while (count < jsonArray.length()) {
                JSONObject oneObject = jsonArray.getJSONObject(count);
                // Pulling items from the array
                bookingId = oneObject.getInt("bookingId");
                courseName = oneObject.getString("courseName");
                day = oneObject.getString("day");
                hour = oneObject.getString("hour");
                teacherDatas = oneObject.getString("teacherDatas");
                status = oneObject.getString("status");

                Operation userReservationsData = new Operation(bookingId, courseName, day, hour, teacherDatas, status);
                reservationsListAdapter.add(userReservationsData);
                count++;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}


