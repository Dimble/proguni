package com.example.mybookings;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ReservationsListAdapter extends ArrayAdapter<Operation> {
    private List list = new ArrayList();
    private Operation operations = null;
    private ReservationsListAdapter.OperationHolder operationHolder = null;
    private int deletionId = -1;

    public ReservationsListAdapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
    public void add(Operation object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Operation getItem(int position) {
        return (Operation) list.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View row = convertView;
        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.adapter_view_layout, parent, false);
            operationHolder = new OperationHolder();
            operationHolder.tvReservationCourse = row.findViewById(R.id.tvCourseRes);
            operationHolder.tvReservationDay = row.findViewById(R.id.tvDayRes);
            operationHolder.ivInfos = row.findViewById(R.id.ivInfosRes);
            operationHolder.ivDelete = row.findViewById(R.id.ivDeleteRes);

            row.setTag(operationHolder);

        } else {
            operationHolder = (ReservationsListAdapter.OperationHolder) row.getTag();
        }

        operations = (Operation) this.getItem(position);

        operationHolder.tvReservationCourse.setText(operations.getCourseName());
        operationHolder.tvReservationDay.setText(operations.getDay() + " || " + operations.getHour());
        operationHolder.ivDelete.setTag(operations.getBookingId());

        operationHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletionId =(int) getItem(position).getBookingId();
                new DeleteItemTask(deletionId).execute();
            }
        });


        operationHolder.ivInfos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ReservationsInfos.class);
                Bundle extras = new Bundle();
                extras.putString("EXTRA_COURSE", getItem(position).getCourseName());
                extras.putString("EXTRA_DAY", getItem(position).getDay());
                extras.putString("EXTRA_HOUR", getItem(position).getHour());
                extras.putString("EXTRA_TEACHER", getItem(position).getTeacherDatas());
                extras.putString("EXTRA_STATUS", getItem(position).getStatus());
                intent.putExtras(extras);
                getContext().startActivity(intent);
            }
        });

        return row;
    }

    static class OperationHolder {
        TextView tvReservationCourse, tvReservationDay = null;
        ImageView ivInfos, ivDelete = null;
    }

    public class DeleteItemTask extends AsyncTask<Void, Void, String> {

        String strUrl = null;
        int id = -1;

        public DeleteItemTask(int deletionId){
            this.id = deletionId;
        }

        @Override
        protected void onPreExecute() {
            strUrl = "http://192.168.1.3:8080/Bookings/BookingController?action=delete_reservation&reservationId=" + id;
        }

        @Override
        protected void onPostExecute(String s) {
            Intent intent = new Intent(getContext(), BookingDeleted.class);
            getContext().startActivity(intent);
        }

        @Override
        protected String doInBackground(Void... voids) {
            String jsonString = null;
            try {
                URL url = new URL(strUrl);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.connect();

                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();

                while ((jsonString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(jsonString + "\n");
                }

                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();

                return stringBuilder.toString().trim();

            } catch (IOException exception) {
                System.out.println(exception.getMessage());
            }
            return null;
        }
    }

}
