package priorityqueue;

import java.util.*;

public class PriorityQueue<T extends Comparable<T>> {

    // Number of the elements in the heap.
    private int heapSize = 0;

    // ArrayList size may be > heapSize
    private int heapCapacity = 0;

    private ArrayList<T> heap = null;

    /**
     * If we are storing one element at index i, its parent will be stored at index
     * i/2 (unless its a root, as root has no parent) and can be access by
     * arrList.get(i/2).
     * 
     * @return parent index.
     */

    private static int getParent(int index) {
        return index / 2;
    }

    /**
     * This method find the left child position.
     * 
     * @return left child index.
     */

    private static int getLeftChild(int index) {
        return (index * 2) + 1;
    }

    /**
     * This method find the right child position.
     * 
     * @return right child index.
     */

    private static int getRightChild(int index) {
        return (index * 2) + 2;
    }

    /**
     * The swap() method using two given index, swap two elements of the heap.
     * 
     * @param firstIndex  the index of the first element we want to swap.
     * @param secondIndex the index of the second element we want to swap.
     */

    private void swap(int firstIndex, int secondIndex) {
        T firstElement = heap.get(firstIndex);
        T secondElement = heap.get(secondIndex);

        heap.set(firstIndex, secondElement);
        heap.set(secondIndex, firstElement);
    }

    /**
     * This method gives the PriorityQueue size.
     * 
     * @return the PriorityQueue size.
     */
    public int size() {
        return heapSize;
    }

    /**
     * This method verify if the PriorityQueue is empty.
     * 
     * @return true if it's empty, false otherwise.
     */
    public boolean isEmpty() {
        return heapSize == 0;
    }

    /**
     * PQ Constructor:
     */

    public PriorityQueue() {
        this(1);
    }

    public PriorityQueue(int size) {
        heap = new ArrayList<T>(size);
    }

    /**
     * PQ Constructor:
     *
     * PriorityQueue filled with known given elements.
     */
    public PriorityQueue(ArrayList<T> elements) {
        heapSize = elements.size();
        heapCapacity = elements.size();
        heap = new ArrayList<T>(heapCapacity);

        // riempo l'heap
        for (int i = 0; i < heapSize; i++) {
            heap.add(elements.get(i));
        }

        // faccio Heapify
        for (int i = 0; i >= heapSize; i++) {
            minHeapify(heap, i);
        }
    }

    /**
     * minHeapify()
     * 
     * In this type of heap, the value of parent node will always be less than or
     * equal to the value of child node across the tree and the node with lowest
     * value will be the root node of the tree.
     * 
     * Complexity: O (log N).
     * 
     * @param arrList ArrayList containing the elements we are working on.
     * @param i       starting index.
     */
    public void minHeapify(ArrayList<T> arrList, int i) {
        int left = getLeftChild(i);
        int right = getRightChild(i);
        int smallest = i;

        if (left <= arrList.size() - 1 && arrList.get(left).compareTo(arrList.get(i)) < 0)
            smallest = left;
        if (right <= arrList.size() - 1 && arrList.get(right).compareTo(arrList.get(smallest)) < 0)
            smallest = right;
        if (smallest != i) {
            swap(i, smallest);
            minHeapify(arrList, smallest);
        }
    }

    /**
     * This method adds the specified element to the PriorityQueue in the right
     * position.
     * 
     * @param element: the element to be added.
     */

    public void insert(T element) {
        if (element == null)
            throw new IllegalArgumentException();

        heap.add(element);
        int index = heap.size() - 1;

        // swapping the added element with his father until the invariant is respected
        // again
        while (index > 0 && heap.get(index).compareTo(heap.get(getParent(index))) < 0) {
            swap(index, getParent(index));
            index = getParent(index);
        }
    }

    /**
     * minimum()
     * 
     * @return the element with the smallest key.
     */

    public T minimum() {
        if (heap.size() <= 0)
            return null; 
        else
            return heap.get(0);
    }

    /**
     * Delete and returns the PriorityQueue's element with the smallest key
     * 
     * @return minValue
     */

    public T extractMin() {
        if (heap.size() <= 0)
            return null;
        else {
            T minValue = heap.get(0);
            // moving last element at position 0
            heap.set(0, heap.get(heap.size() - 1));
            heap.remove(heap.size() - 1);
            // respecting invariant again
            minHeapify(heap, 0);
            return minValue;
        }
    }

    /**
     * Get the element at the given index.
     * 
     * @param index Index of the element we want to get.
     * @return the element at the given position.
     */

    public T getObj(int index) {
        return heap.get(index);
    }

    /**
     * decreaseKey()
     * 
     * decreases the value of the key of the element X to the new one K which is
     * supposed to be at least equal to the current value of the key of the element
     * X.
     * 
     * @param i   which is the index of the element X.
     * @param key which is the new key value.
     */

    public void decreaseKey(int i, T key) {
        if (key.compareTo(heap.get(i)) > 0)
            System.out.println("\nthis new key is greater than the current one");
            heap.set(i, key);
        while (i > 0 && (heap.get(getParent(i)).compareTo(heap.get(i)) > 0)) {
            swap(heap.indexOf(heap.get(i)), heap.indexOf(heap.get(getParent(i))));
            i = getParent(i);
        }
    }

    /*************
     * Utility
     ************/

    /**
     * Tests if the value of node i <= node j. This method assumes i & j are valid
     * indices.
     * 
     * @param i Index of the first element we want to compare.
     * @param j Index of the second element we want to compare.
     * @return true i <= node j, false otherwise.
     */
    public boolean less(int i, int j) {
        T node1 = heap.get(i);
        T node2 = heap.get(j);
        return node1.compareTo(node2) <= 0;
    }

    /**
     * Perform bottom up node swim.
     * 
     * @param k Index of the starting element we want to bottom up swim.
     */
    private void swim(int k) {

        // Grab the index of the next parent node to k
        int parent = getParent(k);

        // Keep swimming while we have not reached the
        // root and while we're less than our parent.
        while (k > 0 && less(k, parent)) {
            // Exchange k with the parent
            swap(parent, k);
            k = parent;

            // Grab the index of the next parent node WRT to k
            parent = getParent(k);
        }
    }

    /**
     * Removes the root of the heap
     * 
     * @return the removed element.
     */
    public T removeRoot() {
        return removeAt(0);
    }

    /**
     * Removes a node at a given index
     * 
     * @param i Index of the element we want to remove.
     * @return the removed element.
     */
    public T removeAt(int i) {
        if (isEmpty())
            return null;

        heapSize--;
        T removed_data = heap.get(i);
        swap(i, heapSize);

        // set the value
        heap.set(heapSize, null);

        // Check if the last element was removed
        if (i == heapSize)
            return removed_data;
        T elem = heap.get(i);

        // Try minHeapify element
        minHeapify(heap, i);

        // If minHeapify did not work try swimming
        if (heap.get(i).equals(elem))
            swim(i);
        return removed_data;
    }

}