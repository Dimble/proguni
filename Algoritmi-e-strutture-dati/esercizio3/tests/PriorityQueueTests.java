package tests;

import static org.junit.Assert.*;

import org.junit.Test;
import priorityqueue.PriorityQueue;
import java.util.ArrayList;
import java.util.Comparator;
import org.junit.Before;
import java.lang.*;

public class PriorityQueueTests {
  PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();

  @Before
  /**
   * Runs before each test to clear the PriorityQueue
   */
  public void reset() {
    priorityQueue = new PriorityQueue<Integer>();
  }

  @Test
  public void emptyPQ() {
    if (priorityQueue.isEmpty())
      System.out.println("\nTest PASSED: PriorityQueue is empty.");
    else
      fail("\nTest FAILED: PriorityQueue is not empty.");
  }

  @Test
  public void insert() {
    priorityQueue.insert(30);
    if (priorityQueue.isEmpty())
      System.out.println("\nTest PASSED: PriorityQueue insert(). Added number is: " + priorityQueue.minimum());
    else
      System.out.println("\nTest FAILED: items has not been added to the PriorityQueue\n");
  }

  @Test
  public void minTest() {

    priorityQueue.insert(1);
    priorityQueue.insert(23);
    priorityQueue.insert(65);

    System.out.println("\nTesting minumum()");
    assertEquals((long) 1, (long) priorityQueue.minimum());

  }

  @Test
  public void extractMinTest() {

    priorityQueue.insert(1);
    priorityQueue.insert(23);
    priorityQueue.insert(65);

    System.out.println("\nTesting extractMin()");
    assertEquals((long) 1, (long) priorityQueue.extractMin());
    System.out.println("Verifying if the element has been deleted: " + priorityQueue.isEmpty());
  }

  @Test
  public void decreaseKeyTest() {

    priorityQueue.insert(3141);
    priorityQueue.insert(2123);
    priorityQueue.insert(64325);
    priorityQueue.insert(1534);
    priorityQueue.insert(2344);

    System.out.println("\nTesting decreaseKey()");
    priorityQueue.decreaseKey(2, 4);
    assertEquals((long) 4, (long) priorityQueue.extractMin());
  }

  @Test
  public void illegalArgument() {
    try {
      priorityQueue.insert(null);
      fail("\nTest FAILED: PriorityQueue did not throw IllegalArgumentException when inserting null value.");
    } catch (IllegalArgumentException e) {
      System.out.println("\nTest PASSED: PriorityQueue throws IllegalArgumentException when inserting null value.");
    }
  }

  @Test
  public void removeRootTest() {
    priorityQueue.insert(7);
    priorityQueue.insert(11);
    priorityQueue.insert(26);

    System.out.println("\nTesting removeRoot()");
    priorityQueue.removeRoot();
    assertEquals((long) 7, (long) priorityQueue.extractMin());
  }

  @Test
  public void removeAtTest() {
    priorityQueue.insert(7);
    priorityQueue.insert(4);
    priorityQueue.insert(26);

    System.out.println("\nTesting removeAt()");
    priorityQueue.removeAt(1);
    assertEquals((long) 4, (long) priorityQueue.extractMin());
  }

  @Test
  public void lessTest() {
    priorityQueue.insert(26);
    priorityQueue.insert(7);
    priorityQueue.insert(4);
    priorityQueue.insert(26);

    System.out.println("\nTesting less()");
    assertTrue(priorityQueue.less(0, 3));
  }

  public static void main(String[] args) {
    org.junit.runner.JUnitCore.main("tests.PriorityQueueTests");
  }
}
