package usersimulation;

import editdistance.EditDistance;
import java.io.*;
import java.util.*;
import java.lang.*;

public class EditDistanceUsage {

    /**
     * Interaction with user: returns a result based on the given algorithmOption.
     * 
     * @param algorithmOption contains the algorithm choices that can be done; This
     *                        value can be: -r for recursive version or -d for
     *                        dynamic version.
     * @param dictionaryWords ArrayList filled with dictionary words we are working
     *                        on.
     * @param quotes          ArrayList filled with quotes we are working on.
     * @param editDistances   Matrix needed for storing edit distances.
     * @param min             Array used to store minimal edit distance.
     */

    public static void algorithmOption(String algorithmOption, ArrayList<String> dictionaryWords,
            ArrayList<String> quotes, int[][] editDistances, int[] min, EditDistance editDistanceObj) {
        switch (algorithmOption) {
        case "-r":
            // After filling the ArrayList 'minEditDistance' with all the edit distances
            // for the i-th word, minimum edit distance will be printed by getting
            // the minimum value of this ArrayList.
            ArrayList<Integer> minEditDistance = new ArrayList<>();
            for (int i = 0; i < quotes.size(); i++) {
                for (int j = 0; j < dictionaryWords.size(); j++) {
                    minEditDistance.add(editDistanceObj.editDistance(quotes.get(i), dictionaryWords.get(j)));
                }
                if ((minEditDistance.get(minEditDistance.indexOf(Collections.min(minEditDistance)))) != 0)
                    System.out.println("Minimum EditDistance for word: " + quotes.get(i) + " is: "
                            + minEditDistance.get(minEditDistance.indexOf(Collections.min(minEditDistance))) + "\n");
                else
                    System.out.println("Minimum EditDistance for word: " + quotes.get(i)
                            + " is: 0.\nThis word doesn't need to be corrected.\n");
                // Removes all of the elements from this list in order to analyze next word edit
                // distances.
                minEditDistance.clear();
            }
            break;
        case "-d":
            editDistanceObj.findEditDistancesDyn(dictionaryWords, quotes, editDistances, min);
            editDistanceObj.printResults(dictionaryWords, quotes, editDistances, min);
            break;
        default:
            System.out.println("\nGiven algorithm option is wrong. (valid options are -r or -d)");
            break;
        }
    }

    /**
     * main: dictionary.txt = args[0] and correctme.txt = args[1].
     * 
     * @param args the command line arguments.
     */

    public static void main(String[] args) {

        if(args.length < 2)
            System.out.println("\nERROR: Wrong command line arguments number.\n");

        EditDistance editDistanceObj = new EditDistance();
        ArrayList<String> dictionaryWords = new ArrayList<>();
        ArrayList<String> quotes = new ArrayList<>();

        try {
            String dictionaryPath = args[0];
            String correctMePath = args[1];

            dictionaryWords = editDistanceObj.fileReader(dictionaryPath);
            quotes = editDistanceObj.fileReader(correctMePath);

            int editDistances[][] = new int[quotes.size()][dictionaryWords.size()];
            int min[] = new int[quotes.size()];

            System.out.println("\nPlease, choose which EditDistance version you'd like to execute.\n");
            System.out.println("type -r for recursive version.\n");
            System.out.println("type -d for dynamic version.\n");

            Scanner input = new Scanner(System.in);
            String algOptions = input.nextLine();

            algorithmOption(algOptions, dictionaryWords, quotes, editDistances, min, editDistanceObj);
            input.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}