package edtests;

import static org.junit.Assert.*;

import org.junit.Test;
import editdistance.EditDistance;
import java.util.ArrayList;
import java.util.Comparator;
import org.junit.Before;
import java.util.Collections;

public class EditDistanceTests {
  EditDistance editDistance = new EditDistance();
  private ArrayList<String> incorrectsWords;
  private ArrayList<String> Dictionary;

  @Before
  public void fillArrays() {
    incorrectsWords = new ArrayList<String>();
    Dictionary = new ArrayList<String>();
    String[] incWords = { "dinosauho", "nano", "ohmyGad", "Cesaa", "modahmodah" };
    String[] correctWords = { "dinosauro", "nani", "ohmyGod", "Cesaaa", "mudahmudah" };
    Collections.addAll(incorrectsWords, incWords);
    Collections.addAll(Dictionary, correctWords);
  }

  @Test
  public void TestBothAreEmptyEditDistance() throws Exception {
    System.out.println(" Testing EditDistance with two empty Strings");
    String empty1, empty2;
    empty1 = "";
    empty2 = "";
    assertEquals(0, editDistance.editDistance(empty1, empty2));
  }

  @Test
  public void TestBothAreEmptyEditDistanceDynamic() throws Exception {
    System.out.println(" Testing EditDistanceDynamic with two empty strings ");
    String empty1, empty2 = "";
    empty1 = "";
    empty2 = "";
    assertEquals(0, editDistance.editDistanceDyn(empty1, empty2, empty1.length(), empty2.length()));
  }

  @Test
  public void TestOneIsEmptyEditDistance() throws Exception {
    System.out.println(" Testing EditDistance with one empty string and a filled one ");
    String empty1 = "";
    String word = Dictionary.get(0);
    // dinosauro length = 9 must return 9 since empty1 to be
    // edited into dinosauro must be entered with all the letters.
    assertEquals(9, editDistance.editDistance(empty1, word));
  }

  @Test
  public void TestOneIsEmptyEditDistanceDynamic() throws Exception {
    System.out.println(" Testing EditDistanceDynamic with one empty string and one String ");
    String empty1 = "";
    String word = Dictionary.get(0);
    // dinosauro length=9 must return 9 since empty1 to be
    // edited into dinosauro must be entered with all the letters
    assertEquals(9, editDistance.editDistanceDyn(empty1, word, empty1.length(), word.length()));
  }

  @Test
  public void TestFindEditDistanceDynamic() throws Exception {
    System.out.println(" Test FindEditDistanceDynamic ");
    int[] minimal = new int[incorrectsWords.size()];
    int[] expectedResult = { 2, 2, 2, 1, 4 };
    int distances[][] = new int[incorrectsWords.size()][Dictionary.size()];
    editDistance.findEditDistancesDyn(Dictionary, incorrectsWords, distances, minimal);
    assertArrayEquals(expectedResult, minimal);
  }

  public static void main(String[] args) {
    org.junit.runner.JUnitCore.main("edtests.EditDistanceTests");
  }

}
