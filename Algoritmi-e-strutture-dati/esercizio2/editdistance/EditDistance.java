package editdistance;

import java.io.*;
import java.util.*;
import java.lang.*;

public class EditDistance {

    /**
     * Recursive version.
     * 
     * @param firstString  our first string we are working on.
     * @param secondString our second string we are working on.
     * 
     * @return the minimum number of operations necessary to transform the string s2
     *         into s1.
     */

    public int editDistance(String firstString, String secondString) {
        int firstLength = firstString.length();
        int secondLength = secondString.length();

        if (firstLength == 0)
            return secondLength;
        if (secondLength == 0)
            return firstLength;
        if (firstString.charAt(0) == secondString.charAt(0))
            return editDistance(firstString.substring(1), secondString.substring(1));

        int canc = 1 + editDistance(firstString, secondString.substring(1));
        int ins = 1 + editDistance(firstString.substring(1), secondString);

        return Math.min(canc, ins);
    }

    /**
     * Dynamic version.
     * 
     * @param firstString  our first string we are working on.
     * @param secondString our second string we are working on.
     * @param firstLength  size of the String containing the word(quotes) we are
     *                     working on.
     * @param secondLength size of the String containing the word (dictionary words)
     *                     we are working on.
     * 
     * @return the minimum number of operations necessary to transform the string s2
     *         into s1.
     */

    public int editDistanceDyn(String firstString, String secondString, int firstLength, int secondLength) {

        // This matrix is used to store subproblems, it will contains how many
        // operations are needed
        // for each insert or delete.

        int matrix[][] = new int[firstLength + 1][secondLength + 1];

        // Filling the matrix
        for (int i = 0; i <= firstLength; i++) {
            for (int j = 0; j <= secondLength; j++) {
                // If first string is empty, min = j
                if (i == 0)
                    matrix[i][j] = j;
                // If second string is empty, min = i
                else if (j == 0)
                    matrix[i][j] = i;
                // If the last characters of the string are equal, continue with the remaining
                // ones.
                else if (firstString.charAt(i - 1) == secondString.charAt(j - 1))
                    matrix[i][j] = matrix[i - 1][j - 1];
                // If they are not equal, search the MIN between Insert and Delete operation.
                else
                    matrix[i][j] = 1 + Math.min(matrix[i][j - 1], matrix[i - 1][j]);
            }
        }
        return matrix[firstLength][secondLength];
    }

    /**
     * Dynamic version helper.
     * 
     * @param dictionary    ArrayList filled with dictionary words we are working
     *                      on.
     * @param correctMe     ArrayList filled with quotes we are working on.
     * @param editDistances Matrix needed for storing edit distances.
     * @param min           Array used to store minimal edit distance.
     * 
     */

    public void findEditDistancesDyn(ArrayList<String> dictionary, ArrayList<String> correctMe, int[][] editDistances,
            int[] min) {
        if (!dictionary.isEmpty() && !correctMe.isEmpty()) {
            String dictionaryWord, quotes;
            for (int i = 0; i < correctMe.size(); i++) {
                for (int j = 0; j < dictionary.size(); j++) {
                    quotes = correctMe.get(i);
                    dictionaryWord = dictionary.get(j);
                    // Matrix containing for every i-th string in the first ArrayList of strings
                    // the corresponding j-th editDistance for strings in second ArrayList of
                    // strings
                    editDistances[i][j] = editDistanceDyn(quotes, dictionaryWord, quotes.length(),
                            dictionaryWord.length());
                }
            }
            // extracting minimal editdistance for every i-th string of the first
            // ArrayList of strings
            for (int i = 0; i < correctMe.size(); i++) {
                min[i] = editDistances[i][0];
                for (int j = 0; j < dictionary.size(); j++) {
                    if (min[i] >= editDistances[i][j])
                        min[i] = editDistances[i][j];
                }
            }
        }
    }

    /**
     * This method prints the edit distance result for DYNAMIC VERSION.
     * 
     * @param dictionary    ArrayList filled with dictionary words we are working
     *                      on.
     * @param correctMe     ArrayList filled with quotes we are working on.
     * @param editDistances Matrix conteining edit distances.
     * @param min           Array containing minimal edit distance.
     * 
     */

    public void printResults(ArrayList<String> dictionary, ArrayList<String> correctMe, int[][] editDistances,
            int[] min) {
        String dictionaryWord, quotes;
        // checks editDistance and prints out the couple i j with
        // editdistance(i,j) = minimal editDistance found
        for (int i = 0; i < correctMe.size(); i++) {
            for (int j = 0; j < dictionary.size(); j++) {
                quotes = correctMe.get(i);
                dictionaryWord = dictionary.get(j);
                if (min[i] == editDistances[i][j] && min[i] != 0) {
                    System.out.println("Minimal EditDistance = " + min[i] + " found for word " + quotes + ".\nWord "
                            + quotes + " can be corrected with string: " + dictionaryWord + ".\n");
                } else if (min[i] == editDistances[i][j] && min[i] == 0) {
                    System.out.println("Minimal EditDistance = " + min[i] + " found for word " + quotes
                            + ".\nThis word doesn't need to be corrected.\n");
                }
            }
        }
    }

    /**
     * Reads a file and add all its elements into an ArrayList preparing it for
     * computation.
     * 
     * @param path file's path.
     * @return ArrayList<String>
     * @throws IOException
     */

    public ArrayList<String> fileReader(String path) throws IOException {
        if (path == null)
            throw new IllegalArgumentException("\npath MUST be NOT-NULL\n");
        File file = new File(path);
        Scanner scanner = new Scanner(file).useDelimiter("\\s");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line;
        ArrayList<String> arrList = new ArrayList<>();
        while (scanner.hasNext()) {
            line = scanner.next();
            line = line.replaceAll("[, .:]", "");
            arrList.add(line);
        }
        reader.close();
        return arrList;
    }
}
