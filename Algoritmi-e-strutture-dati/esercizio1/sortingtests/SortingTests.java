package sortingtests;

import static org.junit.Assert.*;
import org.junit.Test;
import sorting.Sorting;
import sorting.Sorting.IntegerComparator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;

import org.junit.Before;

public class SortingTests {

  private Sorting<Integer> myClass;
  private ArrayList<Integer> arrList;

  @Before
  public void populateArrayList() {
    myClass = new Sorting<Integer>(new IntegerComparator());
    arrList = new ArrayList<Integer>();
    arrList.add(new Integer(11));
    arrList.add(new Integer(1));
    arrList.add(new Integer(170));
    arrList.add(new Integer(2));
    arrList.add(new Integer(3));
    arrList.add(new Integer(58));
  }

  @Test
  public void TestArrayListIsNotEmpty() {
    System.out.println("Testing ArrayList not empty \n");
    assertFalse(arrList.isEmpty());
  }

  @Test
  public void TestInsertionSort() {
    System.out.println("Testing InsertionSort \n");
    ArrayList<Integer> values = new ArrayList<>();
    values.add(new Integer(1));
    values.add(new Integer(2));
    values.add(new Integer(3));
    values.add(new Integer(11));
    values.add(new Integer(58));
    values.add(new Integer(170));

    myClass.insertionSort(arrList);
    assertArrayEquals(values.toArray(), arrList.toArray());
  }

  @Test
  public void TestMergeSort() {
    System.out.println("Testing MergeSort \n");
    ArrayList<Integer> values = new ArrayList<>();
    values.add(new Integer(1));
    values.add(new Integer(2));
    values.add(new Integer(3));
    values.add(new Integer(11));
    values.add(new Integer(58));
    values.add(new Integer(170));

    myClass.mergeSort(arrList, 0, arrList.size() - 1);
    assertArrayEquals(values.toArray(), arrList.toArray());
  }

  @Test
  public void doNothingWithOneElementInsertionSort() {
    System.out.println("Testing one element with InsertionSort \n");
    ArrayList<Integer> values = new ArrayList<>();
    ArrayList<Integer> oneElem = new ArrayList<>();
    values.add(7);
    oneElem.add(7);
    myClass.insertionSort(values);
    assertArrayEquals(values.toArray(), oneElem.toArray());
  }

  @Test
  public void doNothingWithOneElementMergeSort() {
    System.out.println("Testing one element with MergeSort \n");
    ArrayList<Integer> values = new ArrayList<>();
    ArrayList<Integer> oneElem = new ArrayList<>();
    values.add(7);
    oneElem.add(7);
    myClass.mergeSort(values, 0, values.size());
    assertArrayEquals(values.toArray(), oneElem.toArray());
  }

  public static void main(String[] args) {
    org.junit.runner.JUnitCore.main("sortingtests.SortingTest");
  }
}
