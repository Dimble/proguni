package usersimulation;

import sorting.Sorting;
import java.util.*;
import java.io.*;

public class UserSimulation {

  static class LongComparator implements Comparator<Long> {
    @Override
    public int compare(Long i1, Long i2) {
      return Long.compare(i1, i2);
    }
  }

  /**
   * Interaction with user: returns a result based on the given algorithmOption.
   * 
   * @param algorithmOption contains the algorithm choices that can be done; This
   *                        value can be: -i for InsertionSort -m for MergeSort -s
   *                        for the second part of exercise I.
   * @param sortingObj      Sorting object.
   * @param arrList         ArrayList filled with elements we are going to work on
   *                        for InsertionSort and MergeSort.
   * @param sums            ArrayList filled with elements we are going to work on
   *                        for second part of Exercise I.
   */
  public static void algorithmOption(String algorithmOption, Sorting<Long> sortingObj, ArrayList<Long> arrList,
      ArrayList<Long> sums) {
    switch (algorithmOption) {
    case "-i":
      sortingObj.insertionSort(arrList);
      break;
    case "-m":
      sortingObj.mergeSort(arrList, 0, arrList.size() - 1);
      break;
    case "-s":
      sortingObj.mergeSort(arrList, 0, arrList.size() - 1);
      for (int i = 0; i < sums.size(); i++) {
        hasSum(arrList, 0, arrList.size() - 1, sums.get(i));
      }
      break;
    default:
      System.out.println("\nGiven algorithm option is wrong. (valid options are -i, -m or -s)");
      break;
    }
  }

  /**
   * Exercise I - part II
   * 
   * @param arrList ArrayList filled with elements we are going to work on.
   * @param f       Index of the first element.
   * @param l       Index of the last element.
   * @param number  File's numbers we are working on.
   * 
   */
  public static boolean hasSum(ArrayList<Long> arrList, int f, int l, Long number) {
    while (f < l) {
      if (arrList.get(f) + arrList.get(l) == number) {
        System.out.println("\n" + number + " [OK]");
        return true;
      } else if (arrList.get(f) + arrList.get(l) < number)
        f++;
      else
        l--;
    }
    System.out.println("\n" + number + " [X]");
    return false;
  }

  /**
   * main
   * 
   * @param args the command line arguments.
   */
  public static void main(String[] args) {
    final long startTime = System.nanoTime();

    if (args.length < 2)
      System.out.println("\nERROR: Wrong command line arguments number.\n");

    Sorting<Long> sortingObj = new Sorting<Long>(new LongComparator());
    ArrayList<Long> arrList = new ArrayList<Long>();
    ArrayList<Long> sums = new ArrayList<Long>();

    try {
      String path = args[0];
      String s_path = args[1];

      arrList = sortingObj.fileReader(path);
      sums = sortingObj.fileReader(s_path);

      System.out.println("\nReads, sort and print your elements contained into a file.");
      System.out.println("Please, choose which sorting algorithm you'd like to use: \n");
      System.out.println("type -i for InsertionSort\n");
      System.out.println("type -m for MergeSort\n");
      System.out.println("type -s for the second part of Exercise I\n");

      Scanner input = new Scanner(System.in);
      String algOptions = input.nextLine();

      algorithmOption(algOptions, sortingObj, arrList, sums);
      input.close();

      final long duration = System.nanoTime() - startTime;
      System.out.println("\nProgram execution time: " + duration);

    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }
}
