package sorting;

import java.util.ArrayList;
import java.util.Comparator;
import java.io.*;

public class Sorting<T> {
  ArrayList<T> arrList;
  Comparator<? super T> comparator;

  public Sorting(Comparator<? super T> comparator) {
    arrList = new ArrayList<T>();
    this.comparator = comparator;
  }

  /**
   * Insertion sort.
   * 
   * @param arrList ArrayList containing the elements that needs to be sorted.
   * @return ArrayList<T>
   */

  public ArrayList<T> insertionSort(ArrayList<T> arrList) {
    int n = arrList.size();

    for (int i = 1; i < n; i++) {
      T tmp = arrList.get(i); 
      int j = i - 1;

      while (j >= 0 && comparator.compare(arrList.get(j), tmp) == 1) { 
        arrList.set(j + 1, arrList.get(j)); 
        j = j - 1;
      }
      arrList.set(j + 1, tmp);
    }
    return arrList;
  }

  /**
   * Merge sort.
   * 
   * @param arrList ArrayList containing the elements that needs to be sorted.
   * @param f       Index of the first element.
   * @param l       Index of the last element.
   * 
   * @return ArrayList<T>
   * 
   */

  public ArrayList<T> mergeSort(ArrayList<T> arrList, int f, int l) {
    if (arrList.size() != 1) {
      if (f < l) {
        int m = (f + l) / 2;
        mergeSort(arrList, f, m);
        mergeSort(arrList, m + 1, l);
        merge(arrList, f, m, l);
      }
      return arrList;
    } else
      return arrList;
  }

  /**
   * Merge sort helper.
   * 
   * @param arrList ArrayList containing the elements that needs to be sorted.
   * @param f       Index of the first element.
   * @param m       Index of the element in the midpoint.
   * @param l       Index of the last element.
   * 
   * @return ArrayList<T>
   */

  public void merge(ArrayList<T> arrList, int f, int m, int l) {

    // n1 and n2 contains the length of the two subarrays that must be joined.
    int n1 = m - f + 1;
    int n2 = l - m;
    // Left and Right side ArrayList.
    ArrayList<T> L = new ArrayList<T>();
    ArrayList<T> R = new ArrayList<T>();

    for (int i = 0; i < n1; i++)
      L.add(i, arrList.get(f + i));
    for (int j = 0; j < n2; j++)
      R.add(j, arrList.get(m + 1 + j));

    // merge of both Left and Right sides.
    int i = 0, j = 0, k = 0;

    for (k = f; k < l && i < n1 && j < n2; k++) {
      if (comparator.compare(L.get(i), R.get(j)) == -1 || comparator.compare(L.get(i), R.get(j)) == 0) {
        arrList.set(k, L.get(i));
        i++;
      } else {
        arrList.set(k, R.get(j));
        j++;
      }
    }

    while (i < n1) {
      arrList.set(k, L.get(i));
      k++;
      i++;
    }

    while (j < n2) {
      arrList.set(k, R.get(j));
      k++;
      j++;
    }
  }

  /**
   * Reads a file and add all its elements into an ArrayList preparing it for
   * computation.
   * 
   * @param path file's path.
   * @return ArrayList<Long>
   * @throws IOException
   */

  public ArrayList<Long> fileReader(String path) throws IOException {
    if (path == null)
      throw new IllegalArgumentException("\npath MUST be NOT-NULL\n");
    File file = new File(path);
    BufferedReader reader = new BufferedReader(new FileReader(file));
    String line;
    ArrayList<Long> arrList = new ArrayList<Long>();
    while ((line = reader.readLine()) != null) {
      arrList.add(Long.parseLong(line));
    }
    reader.close();
    return arrList;
  }
}
